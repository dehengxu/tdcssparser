body, div, p, h1, h2, h4, h5 {
    margin: 0;
    padding: 0;
}
body {
    font-family: "SimHei";
    font-size: 18px;
   
   background: url(../Images/bg-top.jpg);
}
p {
    font-size: 1.5em;
    text-indent: 2em;
    line-height: 1.5em;
}
hidden {
    display: none !important;
}
no_indent {
    text-indent: 0px;
}
title{
    padding: 28px 0 78px 57px;
    font-size:1.67em;
    color:#502b09;
    background: url(../Images/bg-title.png) left top no-repeat;
    margin:0 auto;
}
copyright{
    background-image:none;
    
    padding-left:5%;
}
copyright{
    text-indent:0;
    font-size: 1.5em;
}
copyright {
    font-size:2.78em;
    margin:48px 0 92px 0;
}
copyright {
    margin-bottom:36px;
}