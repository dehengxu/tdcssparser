package com.tadu.android.view.read.parser.epub.css;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.tadu.read.R;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);
		
		TextView tv = new TextView(this);
		tv.setText("sds");
		setContentView(tv);
		
		InputStream in = MainActivity.class.getClassLoader().getResourceAsStream("Common.css");
		String x = file2String(in,"UTF-8");
		
//		File file = new File("Common.txt");
//		StringBuffer contents = new StringBuffer();
//		BufferedReader reader = null;
//		
//		try {
//			reader = new BufferedReader(new FileReader(file));
//			String text = null;
//			
//			while ((text = reader.readLine()) != null) {
//				contents.append(text).append(System.getProperty("line.separator"));
//			}
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (reader != null) {
//					reader.close();
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		
//		String cssstring = contents.toString();
		//System.out.print(x);
		
		ArrayList<CssInfo> list = new ArrayList<CssInfo>();
		list = getCssInfoList("aaa",x);
		if (list.isEmpty()) {
			System.out.println("hhhhhhh");
			tv.setText("aaa");
		}
		else {
			System.out.println(list.size());
			tv.setText("bbb");
			for(int i = 0;i < list.size();i++) {
				CssInfo info = list.get(i);
				System.out.println("selectorname:");
				for(int j=0; j < info.selectorName.length;j++)
				System.out.println(info.selectorName[j]);
				System.out.println("------>:");
				System.out.println(info.selectorName.length);
			}
		}
	}
	
	public ArrayList<CssInfo> getCssInfoList(String fileRootPath,String content) {
		ArrayList<CssInfo> infoList = null;
		System.out.println("SSSS1SSS");
		//for(int i=0;i<1000;i++){
		//	Log.e("tadu", "��"+i+"��");
			infoList = new CssParser().getCssparser(fileRootPath,content);
		//}
		
		return  infoList;
	}
	
	public static String file2String(InputStream in, String charset) {
		StringBuffer sb = new StringBuffer();
		
		try {
			LineNumberReader reader = new LineNumberReader(new BufferedReader(new InputStreamReader(in,charset)));
			String line;
			
			while ((line = reader.readLine()) != null) {
				sb.append(line).append(System.getProperty("line.separator"));
			}
			reader.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
