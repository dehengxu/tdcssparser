package com.tadu.android.view.read.parser.epub.css;

import java.util.ArrayList;

/**
 * css????��?�? * @author wangyue
 *
 */
public class CssSelectorInfo {
	
	/**派�???��*/
	private ArrayList<String> deriveNameList = new ArrayList<String>();
	
	/**
	 * 添�?派�??��?
	 */
	public void setDeriveList(String [] derives){
		for(int i=0;i<derives.length;i++){
			deriveNameList.add(derives[i]);
		}
	}
	
	/**
	 * 添�?派�??��?
	 * @param deriveList
	 */
	public void setDeriveList(ArrayList<String> deriveList){
		for(int i=0;i<deriveList.size();i++){
			deriveList.add(deriveList.get(i));
		}
	}
	
	/**
	 * ?��?派�???��
	 * @return
	 */
	public ArrayList<String> getDeriveList(){
		return deriveNameList;
	}
	
	/**
	 * ?��?派�??��?
	 * @return
	 */
	public String[] getDeriveString(){
		String [] derives = new String [deriveNameList.size()];
		for(int i=0;i<deriveNameList.size();i++){
			String name = deriveNameList.get(i);
			derives[i] = name;
		}
		return derives;
	}
	
	/**
	 * ?��?派�??��?
	 * @return
	 */
	public String getDeriveS(){
		StringBuilder derive = new StringBuilder();
		for(int i=0;i<deriveNameList.size();i++){
			String name = deriveNameList.get(i);
			derive.append(name+" ");
		}
		return derive.toString();
	}
	
	/**
	 * ???为�?�?	 * @return
	 */
	public boolean isEnd(){
		if(deriveNameList.size() > 1){
			return false;
		}
		return true;
	}
	
	/**
	 * ?��?两个????��??????	 * @param selectorName
	 * @return
	 */
	public boolean isSame(String selectorName[]){
		if(selectorName.length != deriveNameList.size()){
			return false;
		}else{
			for(int i = 0;i<deriveNameList.size();i++){
				if(!deriveNameList.get(i).equals(selectorName[i])){
					return false;
				}
			}
		}
        return true;
	}
	
	/**
	 * �???��???��
	 * @return �??????��?key
	 */
	public String setback(){
		deriveNameList.remove(0);
		return deriveNameList.get(0);
	}
    

}
