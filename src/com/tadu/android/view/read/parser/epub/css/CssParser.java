package com.tadu.android.view.read.parser.epub.css;


import java.util.ArrayList;

public class CssParser {
	private static final String libSoName = "NDK_CSSParser";
	
	public native ArrayList<CssInfo> getCssparser(String fileRootPath, String content);
	
	static {
		System.loadLibrary(libSoName);
	}
}
