package com.tadu.android.view.read.parser.epub.css;

/**
 * css动画对象
 * @author wangyue
 *
 */
public class CssAnimInfo {
	
	private final String keyTranslate = "translate";
	private final String keyScale = "scale";
	
	private final String keyOpacity = "opacity";
	private final String keyTransform = "transform";
	private final String keyWebKitTransform = "-webkit-transform";

	/**动画名*/
	private String animName = "";
	/**动画播放时间*/
	private int animDuration = -1;
	/**动画延迟播放时间*/
	private int animDelay = -1;
	
	/**动画播放次数*/
	private String animItrectionCount = "";
	/**无限循环*/
	public static String animItrectionCountInfinite = "infinite";
	
	/**播放方向*/
	private String animDirection = "";
	/**正向播放*/
	public static String animDirectionNormal = "normal";
	/**反向播放*/
	public static String animDirectionReverse = "reverse";
	/**正向循环正反交替播放*/
	public static String animDirectionAlternate = "alternate";
	/**反向循环正反交替播放*/
	public static String animDirectionAlternateReverse = "alternate-reverse";
	
	/**动画播放效果*/
	private String animFinction = "";
	/**线性效果*/
	public static String animFinctionLiner = "linear";
	/**缓解效果*/
	public static String animFinctionEase = "ease";
	/**渐现效果*/
	public static String animFinctionEaseIn = "ease-in";
	/**渐隐效果*/
	public static String animFinctionEaseOut = "ease-out";
	
	/**初始是否暂停*/
	private String animationPlayState = "";
	/**自动运行*/
	public static String animationPlayStateRunning = "running";
	/**点击运行*/
	public static String animationPlayStatePasued = "pasued";
	
	/**基准点x*/
	private String transformOriginX = "";
	/**基准点y*/
	private String transformOriginY = "";
	
	
	//关键帧
	
	/**起始x坐标*/
	private String fromTranslateX = "";
	/**起始y坐标*/
	private String fromTranslateY = "";
	/**起始x缩放值*/
	private String fromScaleX = "";
	/**起始y缩放值*/
	private String fromScaleY = "";
	/**起始透明度*/
	private String fromOpacity = "";
	
	/**结束x坐标*/
	private String toTranslateX = "";
	/**结束y坐标*/
	private String toTranslateY = "";	
	/**结束x缩放*/
	private String toScaleX = "";
	/**结束y缩放*/
	private String toScaleY = "";
	/**结束透明度*/
	private String toOpacity = "";

	/**
	 * 设置值
	 * @param statusName
	 * @param name
	 * @param value
	 */
	public void setValues(String statusName,String name,String value){
		if(statusName.equals("from")||statusName.equals("0%")){//起始位置
			if(name.equals(keyOpacity)){
				fromOpacity = value;
			}else if(name.equals(keyWebKitTransform)||name.equals(keyTransform)){
				if(value.startsWith(keyTranslate)){
					String values[] = getValues(value);
					
					if(values.length == 1){
						fromTranslateX = values[0];
						fromTranslateY = values[0];
					}else if(values.length == 2){
						fromTranslateX = values[0];
						fromTranslateY = values[1];
					}
				}else if(value.startsWith(keyScale)){
					String values[] = getValues(value);
					
					if(values.length == 1){
						fromScaleX = values[0];
						fromScaleY = values[0];
					}else if(values.length == 2){
						fromScaleX = values[0];
						fromScaleY = values[1];
					}
				}
			}
		}else if(statusName.equals("to")||statusName.equals("100%")){//终止位置
			if(name.equals(keyOpacity)){
				toOpacity = value;
			}else if(name.equals(keyWebKitTransform)||name.equals(keyTransform)){
				if(value.startsWith(keyTranslate)){
					String values[] = getValues(value);
					
					if(values.length == 1){
						toTranslateX = values[0];
						toTranslateY = values[0];
					}else if(values.length == 2){
						toTranslateX = values[0];
						toTranslateY = values[1];
					}
				}else if(value.startsWith(keyScale)){
					String values[] = getValues(value);
					if(values.length == 1){
						toScaleX = values[0];
						toScaleY = values[0];
					}else if(values.length == 2){
						toScaleX = values[0];
						toScaleY = values[1];
					}
				}
			}
		}
	}
	
	/**
	 * 值交换
	 * @param cssAnimInfo
	 */
	public void setCssAnimInfo(CssAnimInfo cssAnimInfo){
		String animName = cssAnimInfo.getAnimName();
		if(animName != null && animName.length()>0){
			this.animName = animName;
		}
		
		int animDuration = cssAnimInfo.getAnimDuration();
		if(animDuration != -1){
			this.animDuration = animDuration;
		}
		
		int animDelay = cssAnimInfo.getAnimDelay();
		if(animDelay != -1){
			this.animDelay = animDelay;
		}
		
		String animItrectionCount = cssAnimInfo.getAnimItrectionCount();
		if(animItrectionCount != null && animItrectionCount.length()>0){
			this.animItrectionCount = animItrectionCount;
		}
		
		String animDirection = cssAnimInfo.getAnimDirection();
		if(animDirection != null && animDirection.length()>0){
			this.animDirection = animDirection;
		}
		
		String animFinction = cssAnimInfo.getAnimFinction();
		if(animFinction != null && animFinction.length()>0){
			this.animFinction = animFinction;
		}
		
		String animationPlayState = cssAnimInfo.getAnimationPlayState();
		if(animationPlayState != null && animationPlayState.length()>0){
			this.animationPlayState = animationPlayState;
		}
		
		String transformOriginX = cssAnimInfo.getTransformOriginX();
		if(transformOriginX != null && transformOriginX.length()>0){
			this.transformOriginX = transformOriginX;
		}
		
		String transformOriginY = cssAnimInfo.getTransformOriginY();
		if(transformOriginY != null && transformOriginY.length()>0){
			this.transformOriginY = transformOriginY;
		}
		
		
		String fromTranslateX = cssAnimInfo.getFromTranslateX();
		if(fromTranslateX != null && fromTranslateX.length()>0){
			this.fromTranslateX = fromTranslateX;
		}
		
		String fromTranslateY = cssAnimInfo.getFromTranslateY();
		if(fromTranslateY != null && fromTranslateY.length()>0){
			this.fromTranslateY = fromTranslateY;
		}
		
		String fromScaleX = cssAnimInfo.getFromScaleX();
		if(fromScaleX != null && fromScaleX.length()>0){
			this.fromScaleX = fromScaleX;
		}
		
		String fromScaleY = cssAnimInfo.getFromScaleY();
		if(fromScaleY != null && fromScaleY.length()>0){
			this.fromScaleY = fromScaleY;
		}
		
		String fromOpacity = cssAnimInfo.getFromOpacity();
		if(fromOpacity != null && fromOpacity.length()>0){
			this.fromOpacity = fromOpacity;
		}
		
		
		String toTranslateX = cssAnimInfo.getToTranslateX();
		if(toTranslateX != null && toTranslateX.length()>0){
			this.toTranslateX = toTranslateX;
		}
		
		String toTranslateY = cssAnimInfo.getToTranslateY();
		if(toTranslateY != null && toTranslateY.length()>0){
			this.toTranslateY = toTranslateY;
		}
		
		String toScaleX = cssAnimInfo.getToScaleX();
		if(toScaleX != null && toScaleX.length()>0){
			this.toScaleX = toScaleX;
		}
		
		String toScaleY = cssAnimInfo.getToScaleY();
		if(toScaleY != null && toScaleY.length()>0){
			this.toScaleY = toScaleY;
		}
		
		String toOpacity = cssAnimInfo.getToOpacity();
		if(toOpacity != null && toOpacity.length()>0){
			this.toOpacity = toOpacity;
		}
		
	}
	
	/**
	 * 截取值
	 * @param value
	 * @return
	 */
	private String[] getValues(String value){
		int startIndex = value.indexOf("(");
		int endIndex = value.indexOf(")");
		return (value.substring(startIndex+1, endIndex)).trim().split(",");
	}

	public String getAnimName() {
		return animName;
	}

	public void setAnimName(String animName) {
		this.animName = animName;
	}

	public int getAnimDuration() {
		return animDuration;
	}

	public void setAnimDuration(int animDuration) {
		this.animDuration = animDuration;
	}

	public int getAnimDelay() {
		return animDelay;
	}

	public void setAnimDelay(int animDelay) {
		this.animDelay = animDelay;
	}

	public String getAnimItrectionCount() {
		return animItrectionCount;
	}

	public void setAnimItrectionCount(String animItrectionCount) {
		this.animItrectionCount = animItrectionCount;
	}

	public String getAnimDirection() {
		return animDirection;
	}

	public void setAnimDirection(String animDirection) {
		this.animDirection = animDirection;
	}

	public String getAnimFinction() {
		return animFinction;
	}

	public void setAnimFinction(String animFinction) {
		this.animFinction = animFinction;
	}

	public String getAnimationPlayState() {
		return animationPlayState;
	}


	public void setAnimationPlayState(String animationPlayState) {
		this.animationPlayState = animationPlayState;
	}


	public String getFromTranslateX() {
		return fromTranslateX;
	}

	public void setFromTranslateX(String fromTranslateX) {
		this.fromTranslateX = fromTranslateX;
	}

	public String getFromTranslateY() {
		return fromTranslateY;
	}

	public void setFromTranslateY(String fromTranslateY) {
		this.fromTranslateY = fromTranslateY;
	}

	public String getFromScaleX() {
		return fromScaleX;
	}

	public void setFromScaleX(String fromScaleX) {
		this.fromScaleX = fromScaleX;
	}

	public String getFromScaleY() {
		return fromScaleY;
	}

	public void setFromScaleY(String fromScaleY) {
		this.fromScaleY = fromScaleY;
	}

	public String getFromOpacity() {
		return fromOpacity;
	}

	public void setFromOpacity(String fromOpacity) {
		this.fromOpacity = fromOpacity;
	}

	public String getToTranslateX() {
		return toTranslateX;
	}

	public void setToTranslateX(String toTranslateX) {
		this.toTranslateX = toTranslateX;
	}

	public String getToTranslateY() {
		return toTranslateY;
	}

	public void setToTranslateY(String toTranslateY) {
		this.toTranslateY = toTranslateY;
	}

	public String getToScaleX() {
		return toScaleX;
	}

	public void setToScaleX(String toScaleX) {
		this.toScaleX = toScaleX;
	}

	public String getToScaleY() {
		return toScaleY;
	}

	public void setToScaleY(String toScaleY) {
		this.toScaleY = toScaleY;
	}

	public String getToOpacity() {
		return toOpacity;
	}

	public void setToOpacity(String toOpacity) {
		this.toOpacity = toOpacity;
	}

	public String getTransformOriginX() {
		return transformOriginX;
	}

	public void setTransformOriginX(String transformOriginX) {
		this.transformOriginX = transformOriginX;
	}

	public String getTransformOriginY() {
		return transformOriginY;
	}

	public void setTransformOriginY(String transformOriginY) {
		this.transformOriginY = transformOriginY;
	}
	
	@Override
	public String toString() {
		String str = "[1.css动画\n"				
				    +"[animName:"+animName+"]\n"
	                +"[animDuration:"+animDuration+"]\n"
	                +"[animDelay:"+animDelay+"]\n"
	                +"[animItrectionCount:"+animItrectionCount+"]\n"
	                +"[animDirection:"+animDirection+"]\n"
	                +"[animFinction:"+animFinction+"]\n"
	                +"[animationPlayState:"+animationPlayState+"]\n"
	                +"[transformOriginX:"+transformOriginX+"]\n"
	                +"[transformOriginY:"+transformOriginY+"]\n"
	                
	                +"[2.关键帧:\n"
	                +"[fromTranslateX:"+fromTranslateX+"]\n"
	                +"[fromTranslateY:"+fromTranslateY+"]\n"
	                +"[fromScaleX:"+fromScaleX+"]\n"
	                +"[fromScaleY:"+fromScaleY+"]\n"
	                +"[fromOpacity:"+fromOpacity+"]\n"
	                
	                +"[toTranslateX:"+toTranslateX+"]\n"
	                +"[toTranslateY:"+toTranslateY+"]\n"
	                +"[toScaleX:"+toScaleX+"]\n"
	                +"[toScaleY:"+toScaleY+"]\n"
	                +"[toOpacity:"+toOpacity+"]\n"

	                ;
		return str;
	}
	
}
