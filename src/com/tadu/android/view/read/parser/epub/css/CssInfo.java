package com.tadu.android.view.read.parser.epub.css;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * css属性对象
 * @author wangyue
 *
 */
public class CssInfo {
		
	/**css选择器对象*/
	private CssSelectorInfo cssSelectorInfo = new CssSelectorInfo();
	/**选择器名*/
	public String selectorName[];

	//css文件对应的字段
	private final String keyBackground = "background";
	private final String keyBackgroundColor = "background-color";
	private final String keyBackgroundRepeat = "background-repeat";
	private final String keyBackgroundPosition = "background-position";
	
	private final String keyBorder = "border";
	private final String keyBorderTop = "border-top";
	private final String keyBorderRight = "border-right";
	private final String keyBorderBottom = "border-bottom";
	private final String keyBorderLeft = "border-left";
	private final String keyBorderRadius = "border-radius";
	private final String keyBorderShadow = "box-shadow";
	
	private final String keyMargin = "margin";
	private final String keyMarginTop = "margin-top";
	private final String keyMarginBottom = "margin-bottom";
	private final String keyMarginLeft = "margin-left";
	private final String keyMarginRight = "margin-right";
	
	private final String keyPadding = "padding";
	private final String keyPaddingTop = "padding-top";
	private final String keyPaddingBottom = "padding-bottom";
	private final String keyPaddingLeft = "padding-left";
	private final String keyPaddingRight = "padding-right";
	
	private final String keyFontColor = "color";
	private final String keyFontSize = "font-size";
    private final String keyLineHeight = "line-height";
	private final String keyFontWeight = "font-weight";
	private final String keyFontStyle = "font-style";
	private final String keyFontdecoration = "text-decoration";
	private final String keyFontFamily = "font-family";
	private final String keyTextIndent = "text-indent";
	private final String keyTextShadow = "text-shadow";
	
	private final String keyWidth = "width";
	private final String keyHeight = "height";
	
	private final String keyFloat = "float";
	private final String keyClear = "clear";
	
	private final String keyPosition = "position";
	private final String keyTop = "top";
	private final String keyRight = "right";
	private final String keyBottom = "bottom";
	private final String keyLeft = "left";
	
	private final String keyDisplay = "display";
	private final String keyTextAlign = "text-align";
	private final String keyVerticalAlign = "vertical-align";
	
	private final String keyAnimation = "animation";
	private final String keyAnimationName = "animation-name";
	private final String keyAnimationDuration = "animation-duration";
	private final String keyAnimationDelay = "animation-delay";
	private final String keyAnimationItrectionCount = "animation-itrection-count";
	private final String keyAnimationDirection = "animation-dircetion";
	private final String keyAnimationTimingFinction = "animation-timing-finction";
	private final String keyAnimationPlayState = "animation-play-state";
	private final String keyAnimationTransformOrigin = "animation-transform-origin";
	
	private final String keyWebKitAnimation = "-webkit-animation";
	private final String keyWebKitAnimationName = "-webkit-animation-name";
	private final String keyWebKitAnimationDuration = "-webkit-animation-duration";
	private final String keyWebKitAnimationDelay = "-webkit-animation-delay";
	private final String keyWebKitAnimationItrectionCount = "-webkit-animation-itrection-count";
	private final String keyWebKitAnimationDirection = "-webkit-animation-dircetion";
	private final String keyWebKitAnimationTimingFinction = "-webkit-animation-timing-finction";
	private final String keyWebKitAnimationPlayState = "-webkit-animation-play-state";
	private final String keyWebKitAnimationTransformOrigin = "-webkit-animation-transform-origin";
	
	
	/**背景图片列表*/
	private ArrayList<CssBgInfo> cssBglist = new ArrayList<CssBgInfo>();
	public static final int backgroundNoRepeat = 0;//不平铺
	public static final int backgroundRepeat = backgroundNoRepeat+1;//xy平铺
	public static final int backgroundRepeatX = backgroundRepeat+1;//x平铺
	public static final int backgroundRepeatY = backgroundRepeatX+1;//y平铺

	//边框属性
	//边框宽度
	private String border_top_width = "";
	private String border_right_width = "";
	private String border_bottom_width = "";
	private String border_left_width = "";
	//边框颜色
	private int border_top_color = -1;
	private int border_right_color = -1;
	private int border_bottom_color = -1;
	private int border_left_color = -1;
	//边框样式
	private String border_top_style = borderStyleSolid;
	private String border_right_style = borderStyleSolid;
	private String border_bottom_style = borderStyleSolid;
	private String border_left_style = borderStyleSolid;
	/**实线边框*/
	public static final String borderStyleSolid = "solid";
	/**点状虚线边框*/
	public static final String borderStyleDotted = "dotted";
	/**虚线边框*/
	public static final String borderStyleDashed = "dashed";	
	//边框圆角
	private String border_radius = "";
	//边框阴影
	private String border_shadow_startX = "";	
	private String border_shadow_startY = "";	
	private String border_shadow_width = "";
	private int border_shadow_color = -1;
	
	//外边距属性
	private String margin_top = "";
	private String margin_bottom = "";
	private String margin_left = "";
	private String margin_right = "";
	
	//内边距属性
	private String padding_top = "";
	private String padding_bottom = "";
	private String padding_left = "";
	private String padding_right = "";
	
	//文字属性
	/**文字颜色*/
	private int font_color = -1;
	/**文字大小*/
	private String font_size = "";
	/**行间距*/
	private String font_line_height = "";
	/**是否粗体*/
	private boolean font_bold = false;
	/**是否斜体*/
	private boolean font_italic = false;
	/**是否下划线*/
	private boolean font_underline = false;
	/**字体样式*/
	private String font_family = "";
	/**首行缩进*/
	private String text_indent = "";
	/**阴影起始点x*/
	private String text_shadow_startX = "";
	/**阴影起始点y*/
	private String text_shadow_startY = "";
	/**阴影宽度*/
	private String text_shadow_width = "";
	/**阴影颜色*/
	private int text_shadow_color = -1;
	
	/**宽*/
	private String width = "";
	/**高*/
	private String height = "";
	
	/**浮动属性*/
	private int float_property = floatPropertyDefault;
	public static final int floatPropertyDefault = 0;
	public static final int floatPropertyLeft = floatPropertyDefault+1;
	public static final int floatPropertyRight = floatPropertyLeft+1;
	/**是否清除浮动*/
	private boolean isClearFloat = false;
	
	
	/**定位属性*/
	private int position = -1;
	public static final int positionDefault = 0;
	public static final int positionRelative = positionDefault+1;
	public static final int positionAbsolute = positionRelative+1;
	private String positionTop = "";
	private String positionRight = "";
	private String positionBottom = "";
	private String positionLeft = "";
	
	
	/**内联元素*/
	private int display = -1;
	public static final int displayInline = 0;
	public static final int displayNone = displayInline+1;
	public static final int displayBlock = displayNone+1;
	public static final int displayInlineBlock = displayBlock+1;
		
	/**水平对齐方式*/
	private int text_align = -1;
	public static final int textAlignLeft = 0;
	public static final int textAlignRight = textAlignLeft+1;
	public static final int textAlignCenter = textAlignRight+1;
	
	/**竖直对齐方式*/
	private int vertical_align = -1;
	public static final int verticalAlignTop = 0;
	public static final int verticalAlignBottom = verticalAlignTop+1;
	public static final int verticalAlignMiddle = verticalAlignBottom+1;
	public static final int verticalAlignTextTop = verticalAlignMiddle+1;
	public static final int verticalAlignSuper = verticalAlignTextTop+1;
	public static final int verticalAlignBaseline = verticalAlignSuper+1;
	public static final int verticalAlignSub = verticalAlignBaseline+1;
	public static final int verticalAlignTextBottom = verticalAlignSub+1;
	
	/**动画类*/
	private CssAnimInfo cssAnimInfo = null;

	
	/**
	 * 设置属性值
	 * @param propertyName
	 * @param values
	 */
	public void setValues(String fileRootPath,String propertyName,String values[],HashMap<String,CssAnimInfo> cssAnimMap){
		//背景的处理
		if(propertyName.equals(keyBackground)){
			CssBgInfo cssBgInfo = new CssBgInfo();
			
			for(int i = 0;i<values.length;i++){
				String value = values[i];
				if(value.startsWith("url")){
					String background_image_path = value.substring(4,value.length()-1);

					if(background_image_path.startsWith(".")){
						background_image_path = background_image_path.substring(3, background_image_path.length());
					}
					background_image_path = fileRootPath+background_image_path;
					
					cssBgInfo.setBackground_image_path(background_image_path);
				}else if(value.startsWith("#")){
					cssBgInfo.setBackground_color(getColor(value));
				}if(value.equals("repeat")){
					cssBgInfo.setBackground_repeat(backgroundRepeat);
				}else if(value.equals("repeat-x")){
					cssBgInfo.setBackground_repeat(backgroundRepeatX);
				}else if(value.equals("repeat-y")){
					cssBgInfo.setBackground_repeat(backgroundRepeatY);
				}else if(value.equals("no-repeat")){
					cssBgInfo.setBackground_repeat(backgroundNoRepeat);
				}else if(value.equals("top")|| value.equals("bottom")|| value.equals("center") || value.equals("left") 
						|| value.equals("right") || value.indexOf("px")!=-1 || value.indexOf("%")!=-1){
					cssBgInfo.setBackground_position_value(value);
				}
			}
			
			cssBglist.add(cssBgInfo);
		}
		//背景颜色的处理
		else if(propertyName.equals(keyBackgroundColor)){
			CssBgInfo cssBgInfo;
			if(cssBglist.size() == 0){
				cssBgInfo = new CssBgInfo();
			}else{
				cssBgInfo = cssBglist.get(0);
			}
			cssBgInfo.setBackground_color(getColor(values[0]));
			
			if(cssBglist.size() == 0){
				cssBglist.add(cssBgInfo);
			}else{
				cssBglist.set(0, cssBgInfo);
			}
		}
		//背景平铺的处理
		else if(propertyName.equals(keyBackgroundRepeat)){
			CssBgInfo cssBgInfo;
			if(cssBglist.size() == 0){
				cssBgInfo = new CssBgInfo();
			}else{
				cssBgInfo = cssBglist.get(0);
			}
			
			String value = values[0];
			if(value.equals("repeat")){
				cssBgInfo.setBackground_repeat(backgroundRepeat);
			}else if(value.equals("repeat-x")){
				cssBgInfo.setBackground_repeat(backgroundRepeatY);
			}else if(value.equals("repeat-y")){
				cssBgInfo.setBackground_repeat(backgroundRepeatY);
			}else if(value.equals("no-repeat")){
				cssBgInfo.setBackground_repeat(backgroundNoRepeat);
			}

			if(cssBglist.size() == 0){
				cssBglist.add(cssBgInfo);
			}else{
				cssBglist.set(0, cssBgInfo);
			}
		}
		//背景图片的锚点
		else if(propertyName.equals(keyBackgroundPosition)){
			CssBgInfo cssBgInfo;
			if(cssBglist.size() == 0){
				cssBgInfo = new CssBgInfo();
			}else{
				cssBgInfo = cssBglist.get(0);
			}
			
			for(int i = 0;i<values.length;i++){
				cssBgInfo.setBackground_position_value(values[i]);
			}
			
			if(cssBglist.size() == 0){
				cssBglist.add(cssBgInfo);
			}else{
				cssBglist.set(0, cssBgInfo);
			}
		}
		
		//边框
		else if(propertyName.equals(keyBorder)){
			for(int i = 0;i<values.length;i++){
				String value = values[i];
				if(value.startsWith("#")){
					border_top_color = getColor(value);
					border_right_color = getColor(value);
					border_bottom_color = getColor(value);
					border_left_color = getColor(value);
				}else if(value.indexOf("px")!= -1 || value.indexOf("%")!= -1){
					border_top_width = value;
					border_right_width = value;
					border_bottom_width = value;
					border_left_width = value;
				}else if(value.equals(borderStyleSolid)){
					border_top_style = borderStyleSolid;
					border_right_style = borderStyleSolid;
					border_bottom_style = borderStyleSolid;
					border_left_style = borderStyleSolid;
				}else if(value.equals(borderStyleDotted)){
					border_top_style = borderStyleDotted;
					border_right_style = borderStyleDotted;
					border_bottom_style = borderStyleDotted;
					border_left_style = borderStyleDotted;
				}else if(value.equals(borderStyleDashed)){
					border_top_style = borderStyleDashed;
					border_right_style = borderStyleDashed;
					border_bottom_style = borderStyleDashed;
					border_left_style = borderStyleDashed;
				}
			}
		}
		else if(propertyName.equals(keyBorderTop)){
			for(int i = 0;i<values.length;i++){
				String value = values[i];
				System.out.print("------------>>");
				System.out.print(value);
				if(value.startsWith("#")){
					border_top_color = getColor(value);
				}else if(value.indexOf("px")!= -1 || value.indexOf("%")!= -1){
					border_top_width = value;
				}else if(value.equals(borderStyleSolid)){
					border_top_style = borderStyleSolid;
				}else if(value.equals(borderStyleDotted)){
					border_top_style = borderStyleDotted;
				}else if(value.equals(borderStyleDashed)){
					border_top_style = borderStyleDashed;
				}
			}
		}
		else if(propertyName.equals(keyBorderRight)){
			for(int i = 0;i<values.length;i++){
				String value = values[i];
				if(value.startsWith("#")){
					border_right_color = getColor(value);
				}else if(value.indexOf("px")!= -1 || value.indexOf("%")!= -1){
					border_right_width = value;
				}else if(value.equals(borderStyleSolid)){
					border_right_style = borderStyleSolid;
				}else if(value.equals(borderStyleDotted)){
					border_right_style = borderStyleDotted;
				}else if(value.equals(borderStyleDashed)){
					border_right_style = borderStyleDashed;
				}
			}
		}
		else if(propertyName.equals(keyBorderBottom)){
			for(int i = 0;i<values.length;i++){
				String value = values[i];
				if(value.startsWith("#")){
					border_bottom_color = getColor(value);
				}else if(value.indexOf("px")!= -1 || value.indexOf("%")!= -1){
					border_bottom_width = value;
				}else if(value.equals(borderStyleSolid)){
					border_bottom_style = borderStyleSolid;
				}else if(value.equals(borderStyleDotted)){
					border_bottom_style = borderStyleDotted;
				}else if(value.equals(borderStyleDashed)){
					border_bottom_style = borderStyleDashed;
				}
			}
		}
		else if(propertyName.equals(keyBorderLeft)){
			for(int i = 0;i<values.length;i++){
				String value = values[i];
				if(value.startsWith("#")){
					border_left_color = getColor(value);
				}else if(value.indexOf("px")!= -1 || value.indexOf("%")!= -1){
					border_left_width = value;
				}else if(value.equals(borderStyleSolid)){
					border_left_style = borderStyleSolid;
				}else if(value.equals(borderStyleDotted)){
					border_left_style = borderStyleDotted;
				}else if(value.equals(borderStyleDashed)){
					border_left_style = borderStyleDashed;
				}
			}
		}
		//边框圆角
		else if(propertyName.equals(keyBorderRadius)){
			String value = values[0];
			border_radius = value;
		}
		//边框阴影
		else if(propertyName.equals(keyBorderShadow)){
			border_shadow_startX = values[0];
			border_shadow_startY = values[1];
			border_shadow_width = values[2];
			border_shadow_color = getColor(values[3]);;
		}
		
		//外边距的处理
		else if(propertyName.equals(keyMargin)){
			if(values.length == 1){
				margin_top = values[0];
				margin_bottom = values[0];
				margin_left = values[0];
				margin_right = values[0];
			}else if(values.length == 2){
				margin_top = values[0];
				margin_bottom = values[0];
				margin_left = values[1];
				margin_right = values[1];
			}else if(values.length == 3){
				margin_top = values[0];
				margin_left = values[1];
				margin_bottom = values[2];
				margin_right = values[1];
			}else if(values.length == 4){
				margin_top = values[0];
				margin_right = values[1];
				margin_bottom = values[2];
				margin_left = values[3];
			}
		}
		else if(propertyName.equals(keyMarginTop)){
			margin_top = values[0];
		}
		else if(propertyName.equals(keyMarginBottom)){
			margin_bottom = values[0];
		}
		else if(propertyName.equals(keyMarginLeft)){
			margin_left = values[0];
		}
		else if(propertyName.equals(keyMarginRight)){
			margin_right = values[0];
		}
		
		//内边距的处理
		else if(propertyName.equals(keyPadding)){
			if(values.length == 1){
				padding_top = values[0];
				padding_bottom = values[0];
				padding_left = values[0];
				padding_right = values[0];
			}else if(values.length == 2){
				padding_top = values[0];
				padding_bottom = values[0];
				padding_left = values[1];
				padding_right = values[1];
			}else if(values.length == 3){
				padding_top = values[0];
				padding_left = values[1];
				padding_bottom = values[2];
				padding_right = values[1];
			}else if(values.length == 4){
				padding_top = values[0];
				padding_right = values[1];
				padding_bottom = values[2];
				padding_left = values[3];
			}
		}
		else if(propertyName.equals(keyPaddingTop)){
			padding_top = values[0];
		}
		else if(propertyName.equals(keyPaddingBottom)){
			padding_bottom = values[0];
		}
		else if(propertyName.equals(keyPaddingLeft)){
			padding_left = values[0];
		}
		else if(propertyName.equals(keyPaddingRight)){
			padding_right = values[0];
		}

		//字体颜色
		else if(propertyName.equals(keyFontColor)){
			font_color = getColor(values[0]);
		}
		//字体大小
		else if(propertyName.equals(keyFontSize)){
			font_size = values[0];   
		}
		//行间距
		else if(propertyName.equals(keyLineHeight)){
			font_line_height = values[0];   
		}
		//字体加粗
		else if(propertyName.equals(keyFontWeight)){
			if(values[0].equals("bold")){
				font_bold = true;
			}
		}
		//字体斜体
		else if(propertyName.equals(keyFontStyle)){
			if(values[0].equals("italic")){
				font_italic = true;
			}
		}
		//字体下划线
		else if(propertyName.equals(keyFontdecoration)){
			if(values[0].equals("underline")){
				font_underline = true;
			}
		}
		//字体样式
		else if(propertyName.equals(keyFontFamily)){
			String ff = values[0];
			if(ff != null && ff.length() > 0){
				font_family = ff.substring(1,(ff.length()-1));//因为字体都是用小括号括住的，需要处理一下
			}
		}
		//文字首行缩进
		else if(propertyName.equals(keyTextIndent)){
			text_indent = values[0];
		}
		//文字阴影
		else if(propertyName.equals(keyTextShadow)){
			text_shadow_startX = values[0];
			text_shadow_startY = values[1];
			text_shadow_width = values[2];
			text_shadow_color = getColor(values[3]);;
		}
		
		//高属性
		else if(propertyName.equals(keyWidth)){
			this.width = values[0];
		}
		//高属性
		else if(propertyName.equals(keyHeight)){
			this.height = values[0];
		}
		
		//浮动属性
		else if(propertyName.equals(keyFloat)){
			String value = values[0];
			if(value.equals("right")){
				this.float_property = floatPropertyRight;
			}else if(value.equals("left")){
				this.float_property = floatPropertyLeft;
			}
		}
		else if(propertyName.equals(keyClear)){
			String value = values[0];
			if(value.equals("both")){
				this.isClearFloat = true;
			}
		}
		
		
		//定位属性
		else if(propertyName.equals(keyPosition)){
			String value = values[0];
			if(value.equals("relative")){
				this.position = positionRelative;
			}else if(value.equals("absolute")){
				this.position = positionAbsolute;
			}
		}
		else if(propertyName.equals(keyTop)){
			this.positionTop = values[0];
		}
		else if(propertyName.equals(keyRight)){
			this.positionRight = values[0];
		}
		else if(propertyName.equals(keyBottom)){
			this.positionBottom = values[0];
		}
		else if(propertyName.equals(keyLeft)){
			this.positionLeft = values[0];
		}
		
		
		//内联元素
		else if(propertyName.equals(keyDisplay)){
			String value = values[0];
			if(value.equals("inline")){
				this.display = displayInline;
			}else if(value.equals("none")){
				this.display = displayNone;
			}else if(value.equals("block")){
				this.display = displayBlock;
			}else if(value.equals("inline-block")){
				this.display = displayInlineBlock;
			}
		}
		//水平对齐方式
		else if(propertyName.equals(keyTextAlign)){
			String value = values[0];
			if(value.equals("left")){
				this.text_align = textAlignLeft;
			}else if(value.equals("right")){
				this.text_align = textAlignRight;
			}else if(value.equals("center")){
				this.text_align = textAlignCenter;
			}
		}
		//水平对齐方式
		else if(propertyName.equals(keyVerticalAlign)){
			String value = values[0];
			if(value.equals("top")){
				this.vertical_align = verticalAlignTop;
			}else if(value.equals("bottom")){
				this.vertical_align = verticalAlignBottom;
			}else if(value.equals("middle")){
				this.vertical_align = verticalAlignMiddle;
			}else if(value.equals("text-top")){
				this.vertical_align = verticalAlignTextTop;
			}else if(value.equals("super")){
				this.vertical_align = verticalAlignSuper;
			}else if(value.equals("baseline")){
				this.vertical_align = verticalAlignBaseline;
			}else if(value.equals("sub")){
				this.vertical_align = verticalAlignSub;
			}else if(value.equals("text-bottom")){
				this.vertical_align = verticalAlignTextBottom;
			}
		}
		
		
		//动画
		else if(propertyName.equals(keyAnimation) || propertyName.equals(keyWebKitAnimation)){
			if(values != null && values.length == 7){
				if(cssAnimInfo == null){
					cssAnimInfo = new CssAnimInfo();
				}
	            
				cssAnimInfo.setCssAnimInfo(cssAnimMap.get(values[0]));
				cssAnimInfo.setAnimName(values[0]);
				cssAnimInfo.setAnimDuration(getTime(values[1]));
				cssAnimInfo.setAnimDelay(getTime(values[2]));
				cssAnimInfo.setAnimItrectionCount(values[3]);
				cssAnimInfo.setAnimDirection(values[4]);
				cssAnimInfo.setAnimFinction(values[5]);
				cssAnimInfo.setAnimationPlayState(values[6]);
			}
		}
		
		else if(propertyName.equals(this.keyAnimationName) || propertyName.equals(keyWebKitAnimationName)){
			if(cssAnimInfo == null){
				cssAnimInfo = new CssAnimInfo();
			}
			String name = values[0];
			cssAnimInfo.setCssAnimInfo(cssAnimMap.get(name));
			cssAnimInfo.setAnimName(values[0]);
		}
		
		else if(propertyName.equals(this.keyAnimationDuration) || propertyName.equals(keyWebKitAnimationDuration)){
			if(cssAnimInfo == null){
				cssAnimInfo = new CssAnimInfo();
			}
			cssAnimInfo.setAnimDuration(getTime(values[0]));
		}
		else if(propertyName.equals(this.keyAnimationDelay) || propertyName.equals(keyWebKitAnimationDelay)){
			if(cssAnimInfo == null){
				cssAnimInfo = new CssAnimInfo();
			}
			cssAnimInfo.setAnimDelay(getTime(values[0]));
		}
		else if(propertyName.equals(this.keyAnimationItrectionCount) || propertyName.equals(keyWebKitAnimationItrectionCount)){
			if(cssAnimInfo == null){
				cssAnimInfo = new CssAnimInfo();
			}
			cssAnimInfo.setAnimItrectionCount(values[0]);
		}
		else if(propertyName.equals(this.keyAnimationDirection) || propertyName.equals(keyWebKitAnimationDirection)){
			if(cssAnimInfo == null){
				cssAnimInfo = new CssAnimInfo();
			}
			cssAnimInfo.setAnimDirection(values[0]);
		}
		else if(propertyName.equals(this.keyAnimationTimingFinction) || propertyName.equals(keyWebKitAnimationTimingFinction)){
			if(cssAnimInfo == null){
				cssAnimInfo = new CssAnimInfo();
			}
			cssAnimInfo.setAnimFinction(values[0]);
		}
		else if(propertyName.equals(this.keyAnimationPlayState) || propertyName.equals(keyWebKitAnimationPlayState)){
			if(cssAnimInfo == null){
				cssAnimInfo = new CssAnimInfo();
			}
			cssAnimInfo.setAnimationPlayState(values[0]);
		}
		
		else if(propertyName.equals(this.keyAnimationTransformOrigin) || propertyName.equals(keyWebKitAnimationTransformOrigin)){
			if(cssAnimInfo == null){
				cssAnimInfo = new CssAnimInfo();
			}
			cssAnimInfo.setToTranslateX(values[0]);
			cssAnimInfo.setToTranslateY(values[1]);
		}

	}
	
    /**
     * 属性复制
     * @param cssPropertyInfo
     */
	public void setCssPropertyInfo(CssInfo cssPropertyInfo){
		//背景
		ArrayList<CssBgInfo> cssBglist = cssPropertyInfo.getCssBglist();
		for(int i = 0;i<cssBglist.size();i++){
			CssBgInfo cssBgInfo = cssBglist.get(i);
			this.cssBglist.add(cssBgInfo);
		}
		
		//边框处理
		int borderTC = cssPropertyInfo.getBorder_top_color();
		if(borderTC != border_top_color){
			this.border_top_color = borderTC;
		}
		String btw = cssPropertyInfo.getBorder_top_width();
		if(btw != null && btw.length()>0){
			this.border_top_width = btw;
		}
		String bts = cssPropertyInfo.getBorder_top_style();
		if(bts != null && bts.length()>0){
			this.border_top_style = bts;
		}
		
		int borderRC = cssPropertyInfo.getBorder_right_color();
		if(borderRC != border_right_color){
			this.border_right_color = borderRC;
		}
		String brw = cssPropertyInfo.getBorder_right_width();
		if(brw != null && brw.length()>0){
			this.border_right_width = brw;
		}
		String brs = cssPropertyInfo.getBorder_right_style();
		if(brs != null && brs.length()>0){
			this.border_right_style = brs;
		}
		
		int borderBC = cssPropertyInfo.getBorder_bottom_color();
		if(borderBC != border_bottom_color){
			this.border_bottom_color = borderBC;
		}
		String bbw = cssPropertyInfo.getBorder_bottom_width();
		if(bbw != null && bbw.length()>0){
			this.border_bottom_width = bbw;
		}
		String bbs = cssPropertyInfo.getBorder_bottom_style();
		if(bbs != null && bbs.length()>0){
			this.border_bottom_style = bbs;
		}
		
		int borderLC = cssPropertyInfo.getBorder_left_color();
		if(borderLC != border_left_color){
			this.border_left_color = borderLC;
		}
		String blw = cssPropertyInfo.getBorder_left_width();
		if(blw != null && blw.length()>0){
			this.border_left_width = blw;
		}
		String bls = cssPropertyInfo.getBorder_left_style();
		if(bls != null && bls.length()>0){
			this.border_left_style = bls;
		}
		
		//边框圆角
		String borderRadius = cssPropertyInfo.getBorder_radius();
		if(borderRadius != null && borderRadius.length()>0){
			this.border_radius = borderRadius;
		}
		//边框阴影
		String borderShadowStartX = cssPropertyInfo.getBorder_shadow_startX();
		if(borderShadowStartX != null && borderShadowStartX.length()>0){
			this.border_shadow_startX = borderShadowStartX;
		}
		String borderShadowStartY = cssPropertyInfo.getBorder_shadow_startY();
		if(borderShadowStartY != null && borderShadowStartY.length()>0){
			this.border_shadow_startY = borderShadowStartY;
		}		
		String borderShadowWidth = cssPropertyInfo.getBorder_shadow_width();
		if(borderShadowWidth != null && borderShadowWidth.length()>0){
			this.border_shadow_width = borderShadowWidth;
		}
		int borderShadowColor = cssPropertyInfo.getBorder_shadow_color();
		if(borderShadowColor != -1){
			this.border_shadow_color = borderShadowColor;
		}

		
		//外边距处理
		String mt = cssPropertyInfo.getMargin_top();
		if(mt != null && mt.length() > 0){
			this.margin_top = mt;
		}
		String mb = cssPropertyInfo.getMargin_bottom();
		if(mb != null && mb.length() > 0){
			this.margin_bottom = mb;
		}
		String ml = cssPropertyInfo.getMargin_left();
		if(ml != null && ml.length() > 0){
			this.margin_left = ml;
		}
		String mr = cssPropertyInfo.getMargin_right();
		if(mr != null && mr.length() > 0){
			this.margin_right = mr;
		}

		//内边距处理
		String pt = cssPropertyInfo.getPadding_top();
		if(pt != null && pt.length() > 0){
			this.padding_top = pt;
		}
		String pb = cssPropertyInfo.getPadding_bottom();
		if(pb != null && pb.length() > 0){
			this.padding_bottom = pb;
		}
		String pl = cssPropertyInfo.getPadding_left();
		if(pl != null && pl.length() > 0){
			this.padding_left = pl;
		}
		String pr = cssPropertyInfo.getPadding_right();
		if(pr != null && pr.length() > 0){
			this.padding_right = pr;
		}

		
		//字体样式处理
		int fc = cssPropertyInfo.getFont_color();
		if(fc != -1){
			this.font_color = fc;
		}
		
		String fs = cssPropertyInfo.getFont_size();
		if(fs != null && fs.length() > 0){
			this.font_size = fs;
		}
		String lh = cssPropertyInfo.getFont_line_height();
		if(lh != null && lh.length() > 0){
			this.font_line_height = lh;
		}

		boolean fb = cssPropertyInfo.isFont_bold();
		if(fb){
			this.font_bold = fb;
		}
		boolean fi = cssPropertyInfo.isFont_italic();
		if(fi){
			this.font_italic = fi;
		}
		boolean fu = cssPropertyInfo.isFont_underline();
		if(fu){
			this.font_underline = fu;
		}
		
		String ff = cssPropertyInfo.font_family;
		if(ff != null && ff.length() > 0){
			this.font_family = ff;
		}
		String ti = cssPropertyInfo.text_indent;
		if(ti != null && ti.length() > 0){
			this.text_indent = ti;
		}
		
		//边框阴影
		String textShadowStartX = cssPropertyInfo.getText_shadow_startX();
		if(textShadowStartX != null && textShadowStartX.length() > 0){
			this.text_shadow_startX = textShadowStartX;
		}
		String textShadowStartY = cssPropertyInfo.getText_shadow_startY();
		if(textShadowStartY != null && textShadowStartY.length() > 0){
			this.text_shadow_startY = textShadowStartY;
		}
		String textShadowWidth = cssPropertyInfo.getText_shadow_width();
		if(textShadowWidth != null && textShadowWidth.length() > 0){
			this.text_shadow_width = textShadowWidth;
		}
		int textShadowColor = cssPropertyInfo.getText_shadow_color();
		if(textShadowColor != -1){
			this.text_shadow_color = textShadowColor;
		}
		
		
		//处理宽高
		String w = cssPropertyInfo.getWidth();
		if(w != null && w.length() > 0){
			this.width = w;
		}
		String h = cssPropertyInfo.getHeight();
		if(h != null && h.length() > 0){
			this.height = h;
		}
		
		//处理浮动
		int fp = cssPropertyInfo.getFloat_property();
		if(fp != float_property){
			this.float_property = fp;
		}
		boolean isClearF = cssPropertyInfo.isClearFloat();
		this.isClearFloat = isClearF;
		
		//处理定位
		int position = cssPropertyInfo.getPosition();
		if(position != -1){
			this.position = position;
		}
		String positionTop = cssPropertyInfo.getPositionTop();
		if(positionTop != null && positionTop.length() > 0){
			this.positionTop = positionTop;
		}
		String positionRight = cssPropertyInfo.getPositionRight();
		if(positionRight != null && positionRight.length() > 0){
			this.positionRight = positionRight;
		}
		String positionBottom = cssPropertyInfo.getPositionBottom();
		if(positionBottom != null && positionBottom.length() > 0){
			this.positionBottom = positionBottom;
		}
		String positionLeft = cssPropertyInfo.getPositionLeft();
		if(positionLeft != null && positionLeft.length() > 0){
			this.positionLeft = positionLeft;
		}
			
		//内联元素
		int display = cssPropertyInfo.getDisplay();
		if(display != -1){
			this.display = display;
		}
		
		//左右对齐
		int ta = cssPropertyInfo.getText_align();
		if(ta != -1){
			this.text_align = ta;
		}
		
		//竖直对齐
		int vertical_align = cssPropertyInfo.getVertical_align();
		if(vertical_align != -1){
			this.vertical_align = vertical_align;
		}
		
		//设置cssAnimInfo值
		CssAnimInfo cssAnimInfo = cssPropertyInfo.getCssAnimInfo();
		if(cssAnimInfo != null){
			if(this.cssAnimInfo == null){
				this.cssAnimInfo = new CssAnimInfo();
			}
			this.cssAnimInfo.setCssAnimInfo(cssAnimInfo);
		}

	}
		
    /**
     * 文本属性复制
     * @param cssPropertyInfo
     */
	public void setTextCssPropertyInfo(CssInfo cssPropertyInfo){
		int fc = cssPropertyInfo.getFont_color();
		if(fc != -1){
			this.font_color = fc;
		}
		
		String fs = cssPropertyInfo.getFont_size();
		if(fs != null && fs.length() > 0){
			this.font_size = fs;
		}
		String lh = cssPropertyInfo.getFont_line_height();
		if(lh != null && lh.length() > 0){
			this.font_line_height = lh;
		}
		
		boolean fb = cssPropertyInfo.isFont_bold();
		if(fb){
			this.font_bold = fb;
		}
		boolean fi = cssPropertyInfo.isFont_italic();
		if(fi){
			this.font_italic = fi;
		}
		boolean fu = cssPropertyInfo.isFont_underline();
		if(fu){
			this.font_underline = fu;
		}
		
		String ff = cssPropertyInfo.font_family;
		if(ff != null && ff.length() > 0){
			this.font_family = ff;
		}
		String ti = cssPropertyInfo.text_indent;
		if(ti != null && ti.length() > 0){
			this.text_indent = ti;
		}
		//边框阴影
		String textShadowStartX = cssPropertyInfo.getText_shadow_startX();
		text_shadow_startX = textShadowStartX;
		String textShadowStartY = cssPropertyInfo.getText_shadow_startY();
		text_shadow_startY = textShadowStartY;
		String textShadowWidth = cssPropertyInfo.getText_shadow_width();
		text_shadow_width = textShadowWidth;
		int textShadowColor = cssPropertyInfo.getText_shadow_color();
		text_shadow_color = textShadowColor;
	}
	
    /**
     * 属性继承
     * @param cssPropertyInfo
     */
	public void setInheritCssPropertyInfo(CssInfo cssPropertyInfo){
		//字体样式处理
		int fc = cssPropertyInfo.getFont_color();
		if(fc != -1){
			this.font_color = fc;
		}
		
		String fs = cssPropertyInfo.getFont_size();
		if(fs != null && fs.length() > 0){
			this.font_size = fs;
		}
		String lh = cssPropertyInfo.getFont_line_height();
		if(lh != null && lh.length() > 0){
			this.font_line_height = lh;
		}
		
		boolean fb = cssPropertyInfo.isFont_bold();
		if(fb){
			this.font_bold = fb;
		}
		boolean fi = cssPropertyInfo.isFont_italic();
		if(fi){
			this.font_italic = fi;
		}
		boolean fu = cssPropertyInfo.isFont_underline();
		if(fu){
			this.font_underline = fu;
		}
		
		String ff = cssPropertyInfo.font_family;
		if(ff != null && ff.length() > 0){
			this.font_family = ff;
		}
		String ti = cssPropertyInfo.text_indent;
		if(ti != null && ti.length() > 0){
			this.text_indent = ti;
		}
		//字体阴影
		String textShadowStartX = cssPropertyInfo.getText_shadow_startX();
		text_shadow_startX = textShadowStartX;
		String textShadowStartY = cssPropertyInfo.getText_shadow_startY();
		text_shadow_startY = textShadowStartY;
		String textShadowWidth = cssPropertyInfo.getText_shadow_width();
		text_shadow_width = textShadowWidth;
		int textShadowColor = cssPropertyInfo.getText_shadow_color();
		text_shadow_color = textShadowColor;
		
		int ta = cssPropertyInfo.getText_align();
		if(ta != -1){
			this.text_align = ta;
		}
	}
	
	/**
	 * 颜色转换
	 * @param value
	 * @return
	 */
	public static int getColor(String value){
		try{
			if(value.startsWith("#")){
				if(value.length() == 7){
					value = "ff"+value.substring(1);
					Long l = Long.parseLong(value, 16);
					return l.intValue();
				}else if(value.length() == 4){
					value = "ff000"+value.substring(1);
					Long l = Long.parseLong(value, 16);
					return l.intValue();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0xff000000;
	}
	
	/**
	 * 时间转换
	 * @param value
	 * @return 返回时间为ms
	 */
	private int getTime(String value){
		try{
			if(value.indexOf("s")!=-1){
				float time = Float.parseFloat(value.substring(0,(value.length()-1)));
				return (int)(time*1000);
			}else if(value.indexOf("ms")!=-1){
				int time = Integer.parseInt(value.substring(0,(value.length()-2)));
				return time;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * 获取选择器
	 * @return
	 */
	private String getSelectorName(){
		StringBuilder sb = new StringBuilder();
		if(selectorName != null && selectorName.length > 0){
			for(int i=0;i<selectorName.length;i++){
				sb.append(selectorName[i]+" ");
			}
		}
		return sb.toString();
	}
	
	private String getBgString(){
		StringBuilder sb = new StringBuilder();
		if(cssBglist != null && cssBglist.size() > 0){
			for(int i=0;i<cssBglist.size();i++){
				sb.append("[background_image_path:"+cssBglist.get(i).getBackground_image_path()+"]\n");
                sb.append("[background_color:"+cssBglist.get(i).getBackground_color()+"]\n");
                sb.append("[background_repeat:"+cssBglist.get(i).getBackground_repeat()+"]\n");
                sb.append("[background_position_value1:"+cssBglist.get(i).getBackground_position_value1()+"]\n");
                sb.append("[background_position_value2:"+cssBglist.get(i).getBackground_position_value2()+"]\n");
			}
		}
		return sb.toString();
	}
	
	@Override
	public String toString() {
		String str = "[选择器:"+this.getCssSelectorInfo().getDeriveS()+"/"+getSelectorName()+"]\n"
			        +"[1.背景样式\n"+getBgString()
			        
	                +"[2.边框样式:\n"
	                +"[border_top_width:"+border_top_width+"]\n"
	                +"[border_right_width:"+border_right_width+"]\n"
	                +"[border_bottom_width:"+border_bottom_width+"]\n"
	                +"[border_left_width:"+border_left_width+"]\n"
	                +"[border_top_color:"+border_top_color+"]\n"
	                +"[border_right_color:"+border_right_color+"]\n"
	                +"[border_bottom_color:"+border_bottom_color+"]\n"
	                +"[border_left_color:"+border_left_color+"]\n"
	                +"[border_top_style:"+border_top_style+"]\n"
	                +"[border_right_style:"+border_right_style+"]\n"
	                +"[border_bottom_style:"+border_bottom_style+"]\n"
	                +"[border_left_style:"+border_left_style+"]\n"
	                +"[border_radius:"+border_radius+"]\n"
	                +"[border_shadow_startX:"+border_shadow_startX+"]\n"
	                +"[border_shadow_startY:"+border_shadow_startY+"]\n"
	                +"[border_shadow_width:"+border_shadow_width+"]\n"
	                +"[border_shadow_color:"+border_shadow_color+"]\n"
	                
	                +"[3.外边距样式:\n"
	                +"[margin_top:"+margin_top+"]\n"
	                +"[margin_bottom:"+margin_bottom+"]\n"
	                +"[margin_left:"+margin_left+"]\n"
	                +"[margin_right:"+margin_right+"]\n"
	                
	                +"[4.内边距样式:\n"
	                +"[padding_top:"+padding_top+"]\n"
	                +"[padding_bottom:"+padding_bottom+"]\n"
	                +"[padding_left:"+padding_left+"]\n"
	                +"[padding_right:"+padding_right+"]\n"
	                
	                +"[5.字体样式:\n"
	                +"[font_color:"+font_color+"]\n"
	                +"[font_size:"+font_size+"]\n"
	                +"[font_line_height:"+font_line_height+"]\n"
	                +"[font_bold:"+font_bold+"]\n"
	                +"[font_italic:"+font_italic+"]\n"
	                +"[font_underline:"+font_underline+"]\n"
	                +"[font_family:"+font_family+"]\n"
	                +"[text_indent:"+text_indent+"]\n"
	                +"[text_shadow_startX:"+text_shadow_startX+"]\n"
	                +"[text_shadow_startY:"+text_shadow_startY+"]\n"
	                +"[text_shadow_width:"+text_shadow_width+"]\n"
	                +"[text_shadow_color:"+text_shadow_color+"]\n"
	                
	                +"[6.宽高样式:\n"
	                +"[width:"+width+"]\n"
	                +"[height:"+height+"]\n"
	                
	                +"[7.浮动样式:\n"
	                +"[float_property:"+float_property+"]\n"
	                +"[isClearFloat:"+isClearFloat+"]\n"
	                
	                +"[8.定位样式:\n"
	                +"[position:"+position+"]\n"
	                +"[positionTop:"+positionTop+"]\n"
	                +"[positionRight:"+positionRight+"]\n"
	                +"[positionBottom:"+positionBottom+"]\n"
	                +"[positionLeft:"+positionLeft+"]\n"
	                
	                +"[9.对齐样式:\n"
	                +"[display:"+display+"]\n"
	                +"[text_align:"+text_align+"]\n"
	                +"[vertical_align:"+vertical_align+"]\n"

	                ;
		return str;
	}
	
	public CssAnimInfo getCssAnimInfo() {
		return cssAnimInfo;
	}
	
	public void setCssAnimInfo(CssAnimInfo cssAnimInfo) {
		this.cssAnimInfo = cssAnimInfo;
	}

	public ArrayList<CssBgInfo> getCssBglist() {
		return cssBglist;
	}

	public int getDisplay() {
		return display;
	}

	public int getVertical_align() {
		return vertical_align;
	}

	public int getPosition() {
		return position;
	}

	public String getPositionTop() {
		return positionTop;
	}

	public String getPositionRight() {
		return positionRight;
	}

	public String getPositionBottom() {
		return positionBottom;
	}

	public String getPositionLeft() {
		return positionLeft;
	}

	public boolean isClearFloat() {
		return isClearFloat;
	}

	public String getText_shadow_startX() {
		return text_shadow_startX;
	}

	public String getText_shadow_startY() {
		return text_shadow_startY;
	}

	public String getText_shadow_width() {
		return text_shadow_width;
	}

	public int getText_shadow_color() {
		return text_shadow_color;
	}

	public String getBorder_radius() {
		return border_radius;
	}

	public String getBorder_shadow_startX() {
		return border_shadow_startX;
	}

	public String getBorder_shadow_startY() {
		return border_shadow_startY;
	}

	public String getBorder_shadow_width() {
		return border_shadow_width;
	}

	public int getBorder_shadow_color() {
		return border_shadow_color;
	}

	public String getText_indent() {
		return text_indent;
	}

	public void setText_indent(String text_indent) {
		this.text_indent = text_indent;
	}

	public String getMargin_top() {
		return margin_top;
	}

	public void setMargin_top(String margin_top) {
		this.margin_top = margin_top;
	}

	public String getMargin_bottom() {
		return margin_bottom;
	}

	public void setMargin_bottom(String margin_bottom) {
		this.margin_bottom = margin_bottom;
	}

	public String getMargin_left() {
		return margin_left;
	}

	public void setMargin_left(String margin_left) {
		this.margin_left = margin_left;
	}

	public String getMargin_right() {
		return margin_right;
	}

	public void setMargin_right(String margin_right) {
		this.margin_right = margin_right;
	}

	public String getPadding_top() {
		return padding_top;
	}

	public void setPadding_top(String padding_top) {
		this.padding_top = padding_top;
	}

	public String getPadding_bottom() {
		return padding_bottom;
	}

	public void setPadding_bottom(String padding_bottom) {
		this.padding_bottom = padding_bottom;
	}

	public String getPadding_left() {
		return padding_left;
	}

	public void setPadding_left(String padding_left) {
		this.padding_left = padding_left;
	}

	public String getPadding_right() {
		return padding_right;
	}

	public void setPadding_right(String padding_right) {
		this.padding_right = padding_right;
	}

	public int getFont_color() {
		return font_color;
	}

	public void setFont_color(int font_color) {
		this.font_color = font_color;
	}

	public String getFont_size() {
		return font_size;
	}

	public void setFont_size(String font_size) {
		this.font_size = font_size;
	}

	public String getFont_line_height() {
		return font_line_height;
	}

	public void setFont_line_height(String font_line_height) {
		this.font_line_height = font_line_height;
	}

	public boolean isFont_bold() {
		return font_bold;
	}

	public void setFont_bold(boolean font_bold) {
		this.font_bold = font_bold;
	}

	public boolean isFont_italic() {
		return font_italic;
	}

	public void setFont_italic(boolean font_italic) {
		this.font_italic = font_italic;
	}

	public boolean isFont_underline() {
		return font_underline;
	}

	public void setFont_underline(boolean font_underline) {
		this.font_underline = font_underline;
	}

	public String getFont_family() {
		return font_family;
	}

	public void setFont_family(String font_family) {
		this.font_family = font_family;
	}

	public int getFloat_property() {
		return float_property;
	}

	public void setFloat_property(int float_property) {
		this.float_property = float_property;
	}

	public String getWidth() {
		return width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public int getText_align() {
		return text_align;
	}

	public void setText_align(int text_align) {
		this.text_align = text_align;
	}

	public String getBorder_top_width() {
		return border_top_width;
	}

	public String getBorder_right_width() {
		return border_right_width;
	}
	
	public String getBorder_bottom_width() {
		return border_bottom_width;
	}

	public String getBorder_left_width() {
		return border_left_width;
	}

	public int getBorder_top_color() {
		return border_top_color;
	}

	public int getBorder_right_color() {
		return border_right_color;
	}

	public int getBorder_bottom_color() {
		return border_bottom_color;
	}

	public int getBorder_left_color() {
		return border_left_color;
	}

	public CssSelectorInfo getCssSelectorInfo() {
		return cssSelectorInfo;
	}

	public String getBorder_top_style() {
		return border_top_style;
	}

	public void setBorder_top_style(String border_top_style) {
		this.border_top_style = border_top_style;
	}

	public String getBorder_right_style() {
		return border_right_style;
	}

	public void setBorder_right_style(String border_right_style) {
		this.border_right_style = border_right_style;
	}

	public String getBorder_bottom_style() {
		return border_bottom_style;
	}

	public void setBorder_bottom_style(String border_bottom_style) {
		this.border_bottom_style = border_bottom_style;
	}

	public String getBorder_left_style() {
		return border_left_style;
	}

	public void setBorder_left_style(String border_left_style) {
		this.border_left_style = border_left_style;
	}

	public void setCssSelectorInfo(CssSelectorInfo cssSelectorInfo) {
		this.cssSelectorInfo = cssSelectorInfo;
	}

	public void setCssBglist(ArrayList<CssBgInfo> cssBglist) {
		this.cssBglist = cssBglist;
	}

	public void setBorder_top_width(String border_top_width) {
		this.border_top_width = border_top_width;
	}

	public void setBorder_right_width(String border_right_width) {
		this.border_right_width = border_right_width;
	}

	public void setBorder_bottom_width(String border_bottom_width) {
		this.border_bottom_width = border_bottom_width;
	}

	public void setBorder_left_width(String border_left_width) {
		this.border_left_width = border_left_width;
	}

	public void setBorder_top_color(int border_top_color) {
		this.border_top_color = border_top_color;
	}

	public void setBorder_right_color(int border_right_color) {
		this.border_right_color = border_right_color;
	}

	public void setBorder_bottom_color(int border_bottom_color) {
		this.border_bottom_color = border_bottom_color;
	}

	public void setBorder_left_color(int border_left_color) {
		this.border_left_color = border_left_color;
	}

	public void setBorder_radius(String border_radius) {
		this.border_radius = border_radius;
	}

	public void setBorder_shadow_startX(String border_shadow_startX) {
		this.border_shadow_startX = border_shadow_startX;
	}

	public void setBorder_shadow_startY(String border_shadow_startY) {
		this.border_shadow_startY = border_shadow_startY;
	}

	public void setBorder_shadow_width(String border_shadow_width) {
		this.border_shadow_width = border_shadow_width;
	}

	public void setBorder_shadow_color(int border_shadow_color) {
		this.border_shadow_color = border_shadow_color;
	}

	public void setText_shadow_startX(String text_shadow_startX) {
		this.text_shadow_startX = text_shadow_startX;
	}

	public void setText_shadow_startY(String text_shadow_startY) {
		this.text_shadow_startY = text_shadow_startY;
	}

	public void setText_shadow_width(String text_shadow_width) {
		this.text_shadow_width = text_shadow_width;
	}

	public void setText_shadow_color(int text_shadow_color) {
		this.text_shadow_color = text_shadow_color;
	}

	public void setClearFloat(boolean isClearFloat) {
		this.isClearFloat = isClearFloat;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setPositionTop(String positionTop) {
		this.positionTop = positionTop;
	}

	public void setPositionRight(String positionRight) {
		this.positionRight = positionRight;
	}

	public void setPositionBottom(String positionBottom) {
		this.positionBottom = positionBottom;
	}

	public void setPositionLeft(String positionLeft) {
		this.positionLeft = positionLeft;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	public void setVertical_align(int vertical_align) {
		this.vertical_align = vertical_align;
	}
	
}
