package com.tadu.android.view.read.parser.epub.css;

/**
 * css��������
 * @author wangyue
 *
 */
public class CssBgInfo {
	
	/**������ɫ*/
	private int background_color = -1;
	/**����ͼƬ·��*/
	private String background_image_path = "";
	/**ƽ�̷�ʽ*/
	private int background_repeat = CssInfo.backgroundNoRepeat;
	
	/**����ê��1*/
	private String bg_position_value1 = "";
	/**����ê��2*/
	private String bg_vertical_value2 = "";
	
	
	public int getBackground_color() {
		return background_color;
	}
	public void setBackground_color(int background_color) {
		this.background_color = background_color;
	}
	public String getBackground_image_path() {
		return background_image_path;
	}
	public void setBackground_image_path(String background_image_path) {
		this.background_image_path = background_image_path;
	}
	public int getBackground_repeat() {
		return background_repeat;
	}
	public void setBackground_repeat(int background_repeat) {
		this.background_repeat = background_repeat;
	}
	public String getBackground_position_value1() {
		return bg_position_value1;
	}

	public String getBackground_position_value2() {
		return bg_vertical_value2;
	}
	public void setBackground_position_value(String background_position_value) {
		if(bg_position_value1 != null && bg_position_value1.length()<=0){
			bg_position_value1 = background_position_value;
		}else if(bg_vertical_value2 != null && bg_vertical_value2.length()<=0){
			bg_vertical_value2 = background_position_value;
		}
	}

}
