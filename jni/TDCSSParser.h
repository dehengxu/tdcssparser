//
//  TDCSSParser.h
//  CSSParser
//
//  Created by WangMing on 13-8-23.
//  Copyright (c) 2013骞�apple. All rights reserved.
//

#ifndef CSSParser_Header_h
#define CSSParser_Header_h

#define PS_CLASS_NONE 0
#define PS_CLASS_LINK 1
#define PS_CLASS_VISITED 2
#define PS_CLASS_ACTIVE 3

#define PS_ELEMENT_NONE 0
#define PS_ELEMENT_FIRST_LETTER 1
#define PS_ELEMENT_FIRST_LINE 2

#ifdef __cplusplus
extern "C" {
#endif


struct td_property {
    char *name;
    char *val;
    int important;
    int count;
    struct td_property *next;
};


struct td_selector {
    char *element_Sel;
    char *id_Sel;
    char *class_Sel;
    char *attribute_Sel;
    char *group_Sel;
    struct td_selector *descendant_Sel;
    struct td_selector *child_Sel;
    int pseudo_class;
    int pseudo_element;
    struct td_property *property;
    struct td_selector *next;
};
    
struct td_animation_rule {
    char *key;
    struct td_property *property;
    struct td_animation_rule *next;
};
    
struct td_keyframes_rule {
    char *name;
    struct td_animation_rule *animation;
    struct td_keyframes_rule *next;
};

struct td_selector_list {
    struct td_selector *selector;
    struct td_keyframes_rule *keyframe;
    struct td_selector_list *next;
};

static  char * testchar = ".hidden";
    
struct td_selector_list* css_parse(const char *buffer, int buf_len);
    
#ifdef __cplusplus
}
#endif

#endif

