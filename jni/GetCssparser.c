#include <string.h>
#include <jni.h>
#include "TDCSSParser.h"
#include <android/log.h>
#include <stdio.h>

//jobject cssInfoList;


/*
 * Class:     com_example_tdcssparser_CSSParser
 * Method:    getCssparser
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
 */
JNIEXPORT jobject JNICALL Java_com_tadu_android_view_read_parser_epub_css_CssParser_getCssparser
  (JNIEnv *env, jobject thiz, jstring fileRootPath, jstring content) {

	jstring cssContent = content;

	if (cssContent == NULL) {
	  	return NULL;
	}

	const char* cssString = (char*)(*env)->GetStringUTFChars(env,cssContent,NULL);
	int len = (*env)->GetStringLength(env,cssContent);

	struct td_selector_list *csslist;
	csslist = css_parse(cssString, strlen(cssString));
	struct td_selector_list *csslist_anim = csslist;
	struct td_selector_list *csslist_sel = csslist;

	__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "css string length  ---- > %d  %d",len,strlen(cssString));
	(*env)->ReleaseStringUTFChars(env,cssContent,cssString);

	//ArrayList object
	jclass cls_ArrayList = (*env)->FindClass(env,"java/util/ArrayList");
	jmethodID construct = (*env)->GetMethodID(env,cls_ArrayList,"<init>","()V");
	jobject cssInfoList = (*env)->NewObject(env,cls_ArrayList,construct,"");
	jmethodID arrayList_add = (*env)->GetMethodID(env,cls_ArrayList,"add","(Ljava/lang/Object;)Z");
	(*env)->DeleteLocalRef(env,cls_ArrayList);
	//(*env)->DeleteLocalRef(env,construct);

	//HashMap
	jclass cls_HashMap = (*env)->FindClass(env,"java/util/HashMap");
	if (cls_HashMap == NULL) {
		printf("Class java/util/HashMap not valid\n\n");
	}
	jmethodID hashmap_init = (*env)->GetMethodID(env,cls_HashMap,"<init>","()V");
	if (hashmap_init ==NULL) {
		printf("method ID hashmap_init not valid\n\n");
	}
	jmethodID hashmap_put = (*env)->GetMethodID(env,cls_HashMap,"put","(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
	if (hashmap_put == NULL) {
		printf("method ID hashmap_put not valid\n\n");
	}
	/**css animMap*/
	jobject cssAnimMap = (*env)->NewObject(env,cls_HashMap,hashmap_init);
	(*env)->DeleteLocalRef(env,cls_HashMap);
	//(*env)->DeleteLocalRef(env,hashmap_init);

	//jstring
	jclass cls_String = (*env)->FindClass(env,"java/lang/String");
	if (cls_String == NULL) {
		printf("Class java/lang/String not valid\n\n");
	}
	jmethodID string_init = (*env)->GetMethodID(env,cls_String,"<init>","([B)V");
	if (string_init == NULL) {
		printf("method ID string_init not valid\n\n");
	}
	jstring encoding = (*env)->NewStringUTF(env,"utf-8");

	//CssInfo class
	jclass cls_cssinfo = (*env)->FindClass(env,"com/tadu/android/view/read/parser/epub/css/CssInfo");
	jmethodID construct_cssinfo = (*env)->GetMethodID(env,cls_cssinfo,"<init>","()V");
	jmethodID cssinfo_setValues = (*env)->GetMethodID(env,cls_cssinfo,"setValues","(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/util/HashMap;)V");  //String ,String ,String ,HashMap<String,CssAnimInfo>
	//selectorname id
	jfieldID cssinfo_selectorname = (*env)->GetFieldID(env,cls_cssinfo,"selectorName","[Ljava/lang/String;");

	//CssAnimInfo object
	jclass cls_cssAniminfo = (*env)->FindClass(env,"com/tadu/android/view/read/parser/epub/css/CssAnimInfo");
	jmethodID construct_cssAniminfo = (*env)->GetMethodID(env,cls_cssAniminfo,"<init>","()V");
	jmethodID cssAniminfo_setValues = (*env)->GetMethodID(env,cls_cssAniminfo,"setValues","(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");

	//CssInfo object
	//jobject obj_cssinfo = (*env)->NewObject(env,cls_cssinfo,construct_cssinfo,"");


	while (csslist_anim != NULL) {
		__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "parser succeed!");

		if(csslist_anim->keyframe) {
			__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "animate:%s",csslist_anim->keyframe->name);

			//animation value
			jobject cssAnimInfo = (*env)->NewObject(env,cls_cssAniminfo,construct_cssAniminfo,"");
			//animation name
			const char* animname = csslist_anim->keyframe->name;
			jbyteArray name_bytes = (*env)->NewByteArray(env,strlen(animname));
			(*env)->SetByteArrayRegion(env,name_bytes,0,strlen(animname),(jbyte*)animname);
//			jstring encoding = (*env)->NewStringUTF("utf-8");
			jstring name_element = (jstring)(*env)->NewObject(env,cls_String,string_init,name_bytes,encoding);

			struct td_animation_rule *anim = csslist_anim->keyframe->animation;
			while (anim != NULL) {
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "--->key:%s",anim->key);
				const char* key = anim->key;
				jbyteArray key_bytes = (*env)->NewByteArray(env,strlen(key));
				(*env)->SetByteArrayRegion(env,key_bytes,0,strlen(key),(jbyte*)key);
//				jstring encoding = (*env)->NewStringUTF("utf-8");
				jstring keyString = (jstring)(*env)->NewObject(env,cls_String,string_init,key_bytes,encoding);

				struct td_property *pro = anim->property;
				while (pro) {
					__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "propertyname:%s   propertyvalue:%s",pro->name,pro->val);

					const char* anim_pro_name = pro->name;
					jbyteArray proname_bytes = (*env)->NewByteArray(env,strlen(anim_pro_name));
					(*env)->SetByteArrayRegion(env,proname_bytes,0,strlen(anim_pro_name),(jbyte*)anim_pro_name);
					jstring str_proname = (jstring)(*env)->NewObject(env,cls_String,string_init,proname_bytes,encoding);

					const char* anim_pro_value = pro->val;
					jbyteArray provalue_bytes = (*env)->NewByteArray(env,strlen(anim_pro_value));
					(*env)->SetByteArrayRegion(env,provalue_bytes,0,strlen(anim_pro_value),(jbyte*)anim_pro_value);
					jstring str_provalue = (jstring)(*env)->NewObject(env,cls_String,string_init,provalue_bytes,encoding);

					//cssAnimInfo.setValue(anim->key,pro->name,pro->val);
					(*env)->CallObjectMethod(env,cssAnimInfo,cssAniminfo_setValues,keyString,str_proname,str_provalue);
					__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "DeleteLocalRef 1");
					(*env)->DeleteLocalRef(env,proname_bytes);
					(*env)->DeleteLocalRef(env,str_proname);
					(*env)->DeleteLocalRef(env,provalue_bytes);
					(*env)->DeleteLocalRef(env,str_provalue);
					pro = pro->next;
				}
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "DeleteLocalRef 2");
				(*env)->DeleteLocalRef(env,key_bytes);
				(*env)->DeleteLocalRef(env,keyString);
				anim = anim->next;
			}
			//cssAnimMap.put(animname,cssAnimInfo);
			(*env)->CallObjectMethod(env,cssAnimMap,hashmap_put,name_element,cssAnimInfo);
			__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "DeleteLocalRef 3");
			(*env)->DeleteLocalRef(env,name_bytes);
			(*env)->DeleteLocalRef(env,name_element);
			(*env)->DeleteLocalRef(env,cssAnimInfo);
		}

		csslist_anim = csslist_anim->next;
	}
	__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "DeleteLocalRef 4");
//	(*env)->DeleteLocalRef(env,hashmap_put);
//	(*env)->DeleteLocalRef(env,string_init);
	(*env)->DeleteLocalRef(env,encoding);
	(*env)->DeleteLocalRef(env,cls_cssAniminfo);
//	(*env)->DeleteLocalRef(env,construct_cssAniminfo);
//	(*env)->DeleteLocalRef(env,cssAniminfo_setValues);

	while (csslist_sel != NULL) {
		struct td_selector *selectora = csslist_sel->selector;
		if(selectora != NULL) {
			__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "is selectorName!");
			//CssInfo cssInfo = new CssInfo();
			jobject cssInfo = (*env)->NewObject(env,cls_cssinfo,construct_cssinfo,"");
			//jfieldID cssinfo_selectorname = (*env)->GetFieldID(env,cls_cssinfo,"selectorName","[Ljava/lang/String;");
			 char* name;
			if(selectora->element_Sel) {
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "elementname:%s",selectora->element_Sel);
				name = selectora->element_Sel;
			}
			else if(selectora->class_Sel) {
				//jstring s = doStringcommand(env,selectora->class_Sel);
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "----classname:%s",selectora->class_Sel);
				printf("------------>%s\n",selectora->class_Sel);
				name = selectora->class_Sel;
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "----classname:%s",name);
			}
			else if(selectora->id_Sel) {
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "idname:%s",selectora->id_Sel);
				name = selectora->id_Sel;
			}
			else if(selectora->attribute_Sel) {
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "attributename:%s",selectora->attribute_Sel);
				name = selectora->attribute_Sel;
			}
			else if(selectora->child_Sel) {
				struct td_selector *childSel = selectora->child_Sel;
				char *name_a;
				char *name_b;
				char *name_c;
				while (childSel->child_Sel) {
					if (childSel->next->element_Sel) {
						name_a = childSel->next->element_Sel;
					}
					else if (childSel->next->class_Sel) {
						name_a = childSel->next->class_Sel;
					}
					else if (childSel->next->id_Sel) {
						name_a = childSel->next->id_Sel;
					}
					else if (childSel->next->attribute_Sel) {
						name_a = childSel->next->attribute_Sel;
					}
					name_a = strcat(">",name_a);
					childSel = childSel->next;
				}

				if (childSel->element_Sel != NULL) {
					name_b = childSel->element_Sel;
				}
				else if (childSel->class_Sel != NULL) {
					name_b = childSel->class_Sel;
				}
				else if (childSel->id_Sel != NULL) {
					name_b = childSel->id_Sel;
				}
				else if (childSel->attribute_Sel != NULL) {
					name_b = childSel->attribute_Sel;
				}

				if (childSel->next->element_Sel != NULL) {
					name_c = childSel->next->element_Sel;
				}
				else if (childSel->next->class_Sel != NULL) {
					name_c = childSel->next->class_Sel;
				}
				else if (childSel->next->id_Sel != NULL) {
					name_c = childSel->next->id_Sel;
				}
				else if (childSel->next->attribute_Sel != NULL) {
					name_c = childSel->next->attribute_Sel;
				}

				if(name_a != NULL) {
					name_c = strcat(">",name_c);
					name = strcat(name_b,name_c);
				}
				else {
					name_a = strcat(">",name_a);
					name_c = strcat(">",name_c);
					name = strcat(strcat(name_b,name_c),name_a);
				}
			}
			else if(selectora->descendant_Sel) {
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "is descendant_Sel");
				struct td_selector *descendant = selectora->descendant_Sel;
				char *name_a;
				char *name_b;
				char *name_c;
				char *name_d;
				while (descendant->descendant_Sel) {
					if (descendant->next->element_Sel) {
						name_a = descendant->next->element_Sel;
					}
					else if (descendant->next->class_Sel) {
						name_a = descendant->next->class_Sel;
					}
					else if (descendant->next->id_Sel) {
						name_a = descendant->next->id_Sel;
					}
					else if (descendant->next->attribute_Sel) {
						name_a = descendant->next->attribute_Sel;
					}

					//name_a = strcat(" ",name_a);
					sprintf(name_d,"%s %s",name_d,name_a);
					descendant = descendant->next;
				}

				if (descendant->element_Sel) {
					name_b = descendant->element_Sel;
				}
				else if (descendant->class_Sel) {
					name_b = descendant->class_Sel;
				}
				else if (descendant->id_Sel) {
					name_b = descendant->id_Sel;
				}
				else if (descendant->attribute_Sel) {
					name_b = descendant->attribute_Sel;
				}

				if (descendant->next->element_Sel) {
					name_c = descendant->next->element_Sel;
				}
				else if (descendant->next->class_Sel) {
					name_c = descendant->next->class_Sel;
				}
				else if (descendant->next->id_Sel) {
					name_c = descendant->next->id_Sel;
				}
				else if (descendant->next->attribute_Sel) {
					name_c = descendant->next->attribute_Sel;
				}

				//sprintf(name,"%s %s %s",name_b,name_c,name_d);
				//__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "descendant_Sel:%s",name);
				if(name_d) {
					sprintf(name,"%s %s %s",name_b,name_c,name_d);
				}
				else {
					sprintf(name,"%s %s",name_b,name_c);
				}
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "descendant_Sel:%s",name);
			}

			//cssInfo.selectorName = name;
			char *selector_token;
			char *names_sel[10];
			selector_token = strtok(name," ");
			names_sel[0] = selector_token;
			int m = 1;
			while(selector_token != NULL) {
				selector_token = strtok(NULL," ");
				names_sel[m] = selector_token;
				m++;
			}
			//jstring sel_name = (*env)->NewStringUTF(env,name);
			jobjectArray name_arr = (*env)->NewObjectArray(env,m-1,cls_String,0);
			//(*env)->SetObjectArrayElement(env,namearr,0,sel_name);
			//(*env)->SetObjectField(env,cssInfo,cssinfo_selectorname,namearr);
			int n;
			for(n=0;n<m-1;n++){
				jstring name_sel = (*env)->NewStringUTF(env,names_sel[n]);
				(*env)->SetObjectArrayElement(env,name_arr,n,name_sel);
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "DeleteLocalRef 5");
				(*env)->DeleteLocalRef(env,name_sel);
			}
			(*env)->SetObjectField(env,cssInfo,cssinfo_selectorname,name_arr);
			(*env)->DeleteLocalRef(env,name_arr);

			struct td_property *propertya = selectora->property;
			while(propertya != NULL) {
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "propertyname:%s   propertyvalue:%s",propertya->name,propertya->val);
				char * value = propertya->val;
				char * token;
				char *values[10];
				token = strtok(value," ");
				values[0] = token;
				int i = 1;
				while (token != NULL) {
//					__android_log_print(ANDROID_LOG_INFO, "JNIMsg", ">>>>>>>>>token:%s",token);
					token = strtok(NULL," ");
					values[i] = token;
					i++;
				}

				jobjectArray arr = (*env)->NewObjectArray(env,i-1,cls_String,0);
				int j;
				for (j = 0; j < i-1;j++) {
					__android_log_print(ANDROID_LOG_INFO, "JNIMsg", ">>>>>>>>>token:%s",values[j]);
					jstring valuestr = (*env)->NewStringUTF(env,values[j]);
					(*env)->SetObjectArrayElement(env,arr,j,valuestr);
					__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "DeleteLocalRef 6");
					(*env)->DeleteLocalRef(env,valuestr);
				}

				jstring property_name = (*env)->NewStringUTF(env,propertya->name);
				(*env)->CallVoidMethod(env,cssInfo,cssinfo_setValues,fileRootPath,property_name,arr,cssAnimMap);
				__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "DeleteLocalRef 7");
				(*env)->DeleteLocalRef(env,property_name);
				//(*env)->DeleteLocalRef(env,arr_cls);
				(*env)->DeleteLocalRef(env,arr);
				propertya = propertya->next;
			}
			//cssInfo.setcssPropertyStyleInfo(cssInfo);

			//cssInfoList.add(cssInfo);
			(*env)->CallBooleanMethod(env,cssInfoList,arrayList_add,cssInfo);
			__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "DeleteLocalRef 8");
			(*env)->DeleteLocalRef(env,cssInfo);
		}

		csslist_sel = csslist_sel->next;
	}
	__android_log_print(ANDROID_LOG_INFO, "JNIMsg", "DeleteLocalRef 9");
//	(*env)->DeleteLocalRef(env,cssinfo_selectorname);
//	(*env)->DeleteLocalRef(env,cssinfo_setValues);
//	(*env)->DeleteLocalRef(env,construct_cssinfo);
	(*env)->DeleteLocalRef(env,cls_cssinfo);
	(*env)->DeleteLocalRef(env,cls_String);
	(*env)->DeleteLocalRef(env,cssAnimMap);
//	(*env)->DeleteLocalRef(env,arrayList_add);

	return cssInfoList;
}
