#ifndef __CSS_LEX_H__
#define __CSS_LEX_H__

void init_yylex(const char *buffer, int buf_len);
void end_yylex(void);

#endif