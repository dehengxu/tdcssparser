/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     UNIMPORTANT_TOK = 258,
     WHITESPACE = 259,
     SGML_CD = 260,
     WEBKIT_KEYFRAMES_RULE_SYM = 261,
     WEBKIT_KEYFRAMES_SYM = 262,
     STRING = 263,
     NUMBER = 264,
     PERCENTAGE = 265,
     LENGTH = 266,
     EMS = 267,
     EXS = 268,
     IDENT = 269,
     LINK_PSCLASS_AFTER_IDENT = 270,
     VISITED_PSCLASS_AFTER_IDENT = 271,
     ACTIVE_PSCLASS_AFTER_IDENT = 272,
     FIRST_LINE_AFTER_IDENT = 273,
     FIRST_LETTER_AFTER_IDENT = 274,
     HASH_AFTER_IDENT = 275,
     CLASS_AFTER_IDENT = 276,
     LINK_PSCLASS = 277,
     VISITED_PSCLASS = 278,
     ACTIVE_PSCLASS = 279,
     FIRST_LINE = 280,
     FIRST_LETTER = 281,
     IDSEL = 282,
     CLASS = 283,
     URL = 284,
     RGB = 285,
     IMPORTANT_SYM = 286,
     LBRACKET = 287,
     RBRACKET = 288,
     ASTERISK = 289
   };
#endif
/* Tokens.  */
#define UNIMPORTANT_TOK 258
#define WHITESPACE 259
#define SGML_CD 260
#define WEBKIT_KEYFRAMES_RULE_SYM 261
#define WEBKIT_KEYFRAMES_SYM 262
#define STRING 263
#define NUMBER 264
#define PERCENTAGE 265
#define LENGTH 266
#define EMS 267
#define EXS 268
#define IDENT 269
#define LINK_PSCLASS_AFTER_IDENT 270
#define VISITED_PSCLASS_AFTER_IDENT 271
#define ACTIVE_PSCLASS_AFTER_IDENT 272
#define FIRST_LINE_AFTER_IDENT 273
#define FIRST_LETTER_AFTER_IDENT 274
#define HASH_AFTER_IDENT 275
#define CLASS_AFTER_IDENT 276
#define LINK_PSCLASS 277
#define VISITED_PSCLASS 278
#define ACTIVE_PSCLASS 279
#define FIRST_LINE 280
#define FIRST_LETTER 281
#define IDSEL 282
#define CLASS 283
#define URL 284
#define RGB 285
#define IMPORTANT_SYM 286
#define LBRACKET 287
#define RBRACKET 288
#define ASTERISK 289




/* Copy the first part of user declarations.  */
#line 1 "css_yacc.y"

#include <stdio.h>
#include <string.h>	
#include "css_lex.h"
#include "TDCSSParser.h"
	
#define YYPARSE_PARAM yyparam
#define YYERROR_VERBOSE 1
	


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 15 "css_yacc.y"
{
	char *lexeme;
	char letter;
	struct td_property *property;
	struct td_selector *selector;
	struct td_selector_list *selector_list;
    struct td_descendant_name *descendant_name;
	int pseudo_class;
	int pseudo_element;
    struct td_animation_rule *animation_rule;
    struct td_keyframes_rule *keyframesRule;
}
/* Line 193 of yacc.c.  */
#line 188 "css_yacc.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 201 "css_yacc.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  31
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   260

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  46
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  39
/* YYNRULES -- Number of rules.  */
#define YYNRULES  86
/* YYNRULES -- Number of states.  */
#define YYNSTATES  141

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   289

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    44,    37,    43,    39,    45,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    41,    38,
       2,    40,    42,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    35,     2,    36,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint8 yyprhs[] =
{
       0,     0,     3,     5,     8,    11,    15,    19,    20,    23,
      29,    31,    36,    38,    47,    49,    51,    54,    58,    65,
      67,    69,    71,    76,    79,    81,    83,    85,    87,    90,
      93,    95,    98,   100,   103,   107,   109,   111,   113,   115,
     117,   119,   122,   124,   127,   129,   132,   134,   137,   140,
     142,   144,   149,   158,   160,   162,   164,   166,   168,   174,
     179,   181,   182,   185,   187,   189,   193,   196,   199,   201,
     204,   207,   210,   213,   216,   217,   220,   223,   226,   229,
     232,   235,   238,   240,   243,   246,   249
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      47,     0,    -1,    48,    -1,    50,    49,    -1,    52,    49,
      -1,    50,    49,    48,    -1,    52,    49,    48,    -1,    -1,
      49,     4,    -1,    51,    35,    49,    58,    36,    -1,    60,
      -1,    60,    37,    49,    51,    -1,    53,    -1,     7,    49,
      54,    49,    35,    49,    55,    36,    -1,    14,    -1,     8,
      -1,    56,    49,    -1,    56,    49,    55,    -1,    57,    49,
      35,    49,    58,    36,    -1,    10,    -1,    14,    -1,    75,
      -1,    75,    38,    49,    58,    -1,    60,     4,    -1,    64,
      -1,    59,    -1,    61,    -1,    62,    -1,    64,    73,    -1,
      64,    74,    -1,    74,    -1,    60,     1,    -1,     1,    -1,
      59,    64,    -1,    60,    80,    64,    -1,    71,    -1,    65,
      -1,    66,    -1,    14,    -1,    34,    -1,    67,    -1,    66,
      67,    -1,    27,    -1,    65,    27,    -1,    68,    -1,    65,
      68,    -1,    63,    -1,    39,    14,    -1,    14,    49,    -1,
      14,    -1,     8,    -1,    32,    49,    69,    33,    -1,    32,
      49,    69,    72,    49,    70,    49,    33,    -1,    40,    -1,
      19,    -1,    18,    -1,    26,    -1,    25,    -1,    76,    41,
      49,    78,    77,    -1,    76,    41,    49,    78,    -1,     1,
      -1,    -1,    14,    49,    -1,    31,    -1,    79,    -1,    78,
      82,    79,    -1,    78,     1,    -1,    81,    83,    -1,    83,
      -1,    42,    49,    -1,    43,    49,    -1,    44,    49,    -1,
      45,    49,    -1,    37,    49,    -1,    -1,     9,    49,    -1,
       8,    49,    -1,    10,    49,    -1,    11,    49,    -1,    12,
      49,    -1,    13,    49,    -1,    14,    49,    -1,    84,    -1,
      29,    49,    -1,    30,    49,    -1,    27,    49,    -1,    20,
      49,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   107,   107,   113,   114,   115,   128,   146,   147,   158,
     175,   186,   202,   216,   225,   226,   230,   231,   247,   261,
     262,   270,   271,   284,   288,   289,   290,   293,   296,   304,
     312,   313,   314,   318,   348,   378,   395,   412,   416,   417,
     421,   422,   428,   442,   462,   476,   496,   504,   517,   521,
     522,   527,   541,   558,   562,   563,   567,   581,   599,   607,
     615,   616,   620,   624,   628,   629,   639,   643,   651,   655,
     659,   660,   664,   665,   666,   670,   671,   672,   673,   674,
     675,   676,   677,   678,   679,   683,   688
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "UNIMPORTANT_TOK", "WHITESPACE",
  "SGML_CD", "WEBKIT_KEYFRAMES_RULE_SYM", "WEBKIT_KEYFRAMES_SYM", "STRING",
  "NUMBER", "PERCENTAGE", "LENGTH", "EMS", "EXS", "IDENT",
  "LINK_PSCLASS_AFTER_IDENT", "VISITED_PSCLASS_AFTER_IDENT",
  "ACTIVE_PSCLASS_AFTER_IDENT", "FIRST_LINE_AFTER_IDENT",
  "FIRST_LETTER_AFTER_IDENT", "HASH_AFTER_IDENT", "CLASS_AFTER_IDENT",
  "LINK_PSCLASS", "VISITED_PSCLASS", "ACTIVE_PSCLASS", "FIRST_LINE",
  "FIRST_LETTER", "IDSEL", "CLASS", "URL", "RGB", "IMPORTANT_SYM",
  "LBRACKET", "RBRACKET", "ASTERISK", "'{'", "'}'", "','", "';'", "'.'",
  "'='", "':'", "'>'", "'-'", "'+'", "'/'", "$accept", "stylesheet",
  "rulesets", "maybe_space", "ruleset", "selectors", "keyframerule",
  "keyframes", "keyframe_name", "keyframes_rule", "keyframe_rule", "key",
  "declarations", "selector_with_whitespace", "selector", "descendant",
  "child_selectors", "attribute_name", "simple_selector", "element_name",
  "specifier_list", "specifier", "solitary_class", "attr_name",
  "attr_value", "attribute", "match", "pseudo_element",
  "solitary_pseudo_element", "declaration", "property", "prio", "expr",
  "term", "new_operator", "unary_operator", "operator", "value",
  "hexcolor", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   123,   125,    44,    59,    46,
      61,    58,    62,    45,    43,    47
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    46,    47,    48,    48,    48,    48,    49,    49,    50,
      51,    51,    52,    53,    54,    54,    55,    55,    56,    57,
      57,    58,    58,    59,    60,    60,    60,    60,    60,    60,
      60,    60,    60,    61,    62,    63,    64,    64,    65,    65,
      66,    66,    67,    67,    67,    67,    67,    68,    69,    70,
      70,    71,    71,    72,    73,    73,    74,    74,    75,    75,
      75,    75,    76,    77,    78,    78,    78,    79,    79,    80,
      81,    81,    82,    82,    82,    83,    83,    83,    83,    83,
      83,    83,    83,    83,    83,    84,    84
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     2,     3,     3,     0,     2,     5,
       1,     4,     1,     8,     1,     1,     2,     3,     6,     1,
       1,     1,     4,     2,     1,     1,     1,     1,     2,     2,
       1,     2,     1,     2,     3,     1,     1,     1,     1,     1,
       1,     2,     1,     2,     1,     2,     1,     2,     2,     1,
       1,     4,     8,     1,     1,     1,     1,     1,     5,     4,
       1,     0,     2,     1,     1,     3,     2,     2,     1,     2,
       2,     2,     2,     2,     0,     2,     2,     2,     2,     2,
       2,     2,     1,     2,     2,     2,     2
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    32,     7,    38,    57,    56,    42,     7,    39,     0,
       0,     2,     7,     0,     7,    12,    25,     0,    26,    27,
      46,    24,    36,    37,    40,    44,    35,    30,     0,     0,
      47,     1,     0,     7,     0,    33,    31,    23,     7,     7,
       0,    55,    54,    28,    29,    43,    45,     0,    41,     8,
      15,    14,     7,     7,     0,     5,     0,     6,     0,    69,
      34,     0,    48,    51,    53,     7,    60,     7,     0,    21,
       0,    11,     7,     0,    62,     9,     7,     7,     0,    50,
      49,     7,     0,     0,    19,    20,     0,     7,     7,     0,
      22,     7,     7,     7,     7,     7,     7,     7,     7,     7,
       7,     7,     7,     7,     0,    64,     0,    68,    82,    13,
      16,     0,    52,    76,    75,    77,    78,    79,    80,    81,
      86,    85,    83,    84,    70,    71,    66,    63,     7,     7,
      58,     0,    67,    17,     7,    73,    72,    65,     0,     0,
      18
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    10,    11,    28,    12,    13,    14,    15,    52,    86,
      87,    88,    68,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    54,    81,    26,    65,    43,    27,    69,
      70,   130,   104,   105,    40,   106,   131,   107,   108
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -82
static const yytype_int16 yypact[] =
{
      29,   -82,   -82,   -82,   -82,   -82,   -82,   -82,   -82,   -12,
      16,   -82,   -82,   -15,   -82,   -82,    39,     2,   -82,   -82,
     -82,    79,   -14,    39,   -82,   -82,   -82,   -82,    68,     7,
     -82,   -82,     8,   -82,   158,   -82,   -82,   -82,   -82,   -82,
      39,   -82,   -82,   -82,   -82,   -82,   -82,   -14,   -82,   -82,
     -82,   -82,   -82,   -82,    66,   -82,    13,   -82,   173,    44,
     -82,     6,    44,   -82,   -82,   -82,   -82,   -82,    23,    26,
      38,   -82,   -82,   110,    44,   -82,   -82,   -82,    14,   -82,
     -82,   -82,    13,   137,   -82,   -82,    31,   -82,   -82,    19,
     -82,   -82,   -82,   -82,   -82,   -82,   -82,   -82,   -82,   -82,
     -82,   -82,   -82,   -82,    99,   -82,   230,   -82,   -82,   -82,
      14,    15,   -82,    44,    44,    44,    44,    44,    44,    44,
      44,    44,    44,    44,    44,    44,   -82,   -82,   -82,   -82,
     -82,   205,   -82,   -82,   -82,    44,    44,   -82,    13,    47,
     -82
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -82,   -82,    43,    -7,   -82,    45,   -82,   -82,   -82,    -9,
     -82,   -82,   -81,   -82,   -82,   -82,   -82,   -82,    22,    92,
     -82,    93,   -18,   -82,   -82,   -82,   -82,   -82,    81,   -82,
     -82,   -82,   -82,   -11,   -82,   -82,   -82,    11,   -82
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -75
static const yytype_int16 yytable[] =
{
      29,    90,    30,    36,    46,    32,    37,    34,    -3,     1,
      49,    49,    49,    45,    66,     2,    31,    49,    49,    49,
      33,    53,     3,    49,    84,     9,    56,    67,    85,    46,
       1,    58,    59,     4,     5,     6,     2,   -10,    35,    38,
       7,    72,     8,     3,    39,    61,    62,     9,    49,   -61,
     134,   -61,   112,     3,     4,     5,     6,   139,    73,    75,
      74,     7,    60,     8,    76,    78,     6,   109,     9,    82,
      83,     7,    49,     8,    89,    55,    50,    57,     9,    77,
     110,   111,    51,   140,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,    41,    42,    63,
     126,   133,    44,    71,     4,     5,    64,   -74,   -74,   -74,
     -74,   -74,   -74,   -74,    49,    47,    48,   132,    79,   -74,
     137,   135,   136,     0,    80,     0,   -74,   138,   -74,   -74,
     127,     0,     0,     0,     0,   -59,   128,   -59,     0,     0,
       0,    49,   -74,   -74,   129,    91,    92,    93,    94,    95,
      96,    97,     0,     0,     0,     0,     0,    98,    -4,     1,
       0,     0,    49,     0,    99,     2,   100,   101,     0,     0,
       0,     0,     3,     0,     1,     0,     0,    49,     0,     0,
     102,   103,     0,     4,     5,     6,     0,     3,     0,     0,
       7,     0,     8,     0,     0,     0,     0,     9,     4,     5,
       6,     0,     0,     0,     0,     7,     0,     8,     0,     0,
       0,     0,     9,    91,    92,    93,    94,    95,    96,    97,
       0,     0,     0,     0,     0,    98,     0,     0,     0,     0,
       0,     0,    99,     0,   100,   101,     0,     0,    91,    92,
      93,    94,    95,    96,    97,     0,     0,     0,   102,   103,
      98,     0,     0,     0,     0,     0,     0,    99,     0,   100,
     101
};

static const yytype_int16 yycheck[] =
{
       7,    82,    14,     1,    22,    12,     4,    14,     0,     1,
       4,     4,     4,    27,     1,     7,     0,     4,     4,     4,
      35,    14,    14,     4,    10,    39,    33,    14,    14,    47,
       1,    38,    39,    25,    26,    27,     7,    35,    16,    37,
      32,    35,    34,    14,    42,    52,    53,    39,     4,    36,
      35,    38,    33,    14,    25,    26,    27,   138,    65,    36,
      67,    32,    40,    34,    38,    72,    27,    36,    39,    76,
      77,    32,     4,    34,    81,    32,     8,    34,    39,    41,
      87,    88,    14,    36,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,    18,    19,    33,
       1,   110,    21,    58,    25,    26,    40,     8,     9,    10,
      11,    12,    13,    14,     4,    23,    23,   106,     8,    20,
     131,   128,   129,    -1,    14,    -1,    27,   134,    29,    30,
      31,    -1,    -1,    -1,    -1,    36,    37,    38,    -1,    -1,
      -1,     4,    43,    44,    45,     8,     9,    10,    11,    12,
      13,    14,    -1,    -1,    -1,    -1,    -1,    20,     0,     1,
      -1,    -1,     4,    -1,    27,     7,    29,    30,    -1,    -1,
      -1,    -1,    14,    -1,     1,    -1,    -1,     4,    -1,    -1,
      43,    44,    -1,    25,    26,    27,    -1,    14,    -1,    -1,
      32,    -1,    34,    -1,    -1,    -1,    -1,    39,    25,    26,
      27,    -1,    -1,    -1,    -1,    32,    -1,    34,    -1,    -1,
      -1,    -1,    39,     8,     9,    10,    11,    12,    13,    14,
      -1,    -1,    -1,    -1,    -1,    20,    -1,    -1,    -1,    -1,
      -1,    -1,    27,    -1,    29,    30,    -1,    -1,     8,     9,
      10,    11,    12,    13,    14,    -1,    -1,    -1,    43,    44,
      20,    -1,    -1,    -1,    -1,    -1,    -1,    27,    -1,    29,
      30
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     1,     7,    14,    25,    26,    27,    32,    34,    39,
      47,    48,    50,    51,    52,    53,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    71,    74,    49,    49,
      14,     0,    49,    35,    49,    64,     1,     4,    37,    42,
      80,    18,    19,    73,    74,    27,    68,    65,    67,     4,
       8,    14,    54,    14,    69,    48,    49,    48,    49,    49,
      64,    49,    49,    33,    40,    72,     1,    14,    58,    75,
      76,    51,    35,    49,    49,    36,    38,    41,    49,     8,
      14,    70,    49,    49,    10,    14,    55,    56,    57,    49,
      58,     8,     9,    10,    11,    12,    13,    14,    20,    27,
      29,    30,    43,    44,    78,    79,    81,    83,    84,    36,
      49,    49,    33,    49,    49,    49,    49,    49,    49,    49,
      49,    49,    49,    49,    49,    49,     1,    31,    37,    45,
      77,    82,    83,    55,    35,    49,    49,    79,    49,    58,
      36
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (&yylval, YYLEX_PARAM)
#else
# define YYLEX yylex (&yylval)
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */






/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  /* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;

  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 107 "css_yacc.y"
    {
        *(struct td_selector_list**) yyparam = (yyvsp[(1) - (1)].selector_list);
    ;}
    break;

  case 3:
#line 113 "css_yacc.y"
    { (yyval.selector_list) = (yyvsp[(1) - (2)].selector_list); ;}
    break;

  case 4:
#line 114 "css_yacc.y"
    { (yyval.selector_list) = (yyvsp[(1) - (2)].selector_list);;}
    break;

  case 5:
#line 115 "css_yacc.y"
    {
        struct td_selector_list *pos = (yyvsp[(1) - (3)].selector_list);
        if (pos != NULL) {
            while (pos->next != NULL) {
                pos = pos->next;
            }
            pos->next = (yyvsp[(3) - (3)].selector_list);
        } 
        else {
            (yyvsp[(1) - (3)].selector_list) = (yyvsp[(3) - (3)].selector_list);
        }
        (yyval.selector_list) = (yyvsp[(1) - (3)].selector_list);
    ;}
    break;

  case 6:
#line 128 "css_yacc.y"
    {
        struct td_selector_list *pos = (yyvsp[(1) - (3)].selector_list);
        if (pos != NULL) {
            while (pos->next != NULL) {
                pos = pos->next;
            }
            pos->next = (yyvsp[(3) - (3)].selector_list);
        } 
        else {
            (yyvsp[(1) - (3)].selector_list) = (yyvsp[(3) - (3)].selector_list);
        }
        (yyval.selector_list) = (yyvsp[(1) - (3)].selector_list);
    ;}
    break;

  case 7:
#line 146 "css_yacc.y"
    { ;}
    break;

  case 8:
#line 147 "css_yacc.y"
    { ;}
    break;

  case 9:
#line 158 "css_yacc.y"
    {
        struct td_selector_list *pos = (yyvsp[(1) - (5)].selector_list);
        while (pos != NULL) {
            struct td_property *i = (yyvsp[(4) - (5)].property);
            while (i != NULL) {
                i->count++;
                i = i->next;
            }
            pos->selector->property = (yyvsp[(4) - (5)].property);
            pos = pos->next;
        }
        (yyval.selector_list) = (yyvsp[(1) - (5)].selector_list);
    ;}
    break;

  case 10:
#line 175 "css_yacc.y"
    {
		if ((yyvsp[(1) - (1)].selector) != NULL) {
			(yyval.selector_list) = (struct td_selector_list*)malloc(sizeof(struct td_selector_list));
			(yyval.selector_list)->selector = (yyvsp[(1) - (1)].selector);
            (yyval.selector_list)->keyframe = NULL;
			(yyval.selector_list)->next = NULL;
		}
		else {
			(yyval.selector_list) = NULL;
		}
	;}
    break;

  case 11:
#line 186 "css_yacc.y"
    {//分组选择器 
		if ((yyvsp[(1) - (4)].selector) && (yyvsp[(4) - (4)].selector_list)) {
			struct td_selector_list *new;
			new = (struct td_selector_list*)malloc(sizeof(struct td_selector_list));
			new->selector = (yyvsp[(1) - (4)].selector);
            new->keyframe = NULL;
			new->next = (yyvsp[(4) - (4)].selector_list);
			(yyval.selector_list) = new;
			}
			else {
				(yyval.selector_list) = 0;
			}
	;}
    break;

  case 12:
#line 202 "css_yacc.y"
    {
        if ((yyvsp[(1) - (1)].keyframesRule) != NULL) {
			(yyval.selector_list) = (struct td_selector_list*)malloc(sizeof(struct td_selector_list));
			(yyval.selector_list)->selector = NULL;
            (yyval.selector_list)->keyframe = (yyvsp[(1) - (1)].keyframesRule);
			(yyval.selector_list)->next = NULL;
		}
		else {
			(yyval.selector_list) = NULL;
		}
    ;}
    break;

  case 13:
#line 216 "css_yacc.y"
    {
        (yyval.keyframesRule) = (struct td_keyframes_rule *)malloc(sizeof(struct td_keyframes_rule));
        (yyval.keyframesRule)->name = (yyvsp[(3) - (8)].lexeme);
        (yyval.keyframesRule)->animation = (yyvsp[(7) - (8)].animation_rule);
        (yyval.keyframesRule)->next = NULL;
    ;}
    break;

  case 14:
#line 225 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 15:
#line 226 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 16:
#line 230 "css_yacc.y"
    { (yyval.animation_rule) = (yyvsp[(1) - (2)].animation_rule); ;}
    break;

  case 17:
#line 231 "css_yacc.y"
    {
        struct td_animation_rule *anim = (yyvsp[(1) - (3)].animation_rule);
        if (anim != NULL) {
            while (anim->next != NULL) {
                anim = anim->next;
            }
            anim->next = (yyvsp[(3) - (3)].animation_rule);
        }
        else {
            (yyvsp[(1) - (3)].animation_rule) = (yyvsp[(3) - (3)].animation_rule);
        }
        (yyval.animation_rule) = (yyvsp[(1) - (3)].animation_rule);
    ;}
    break;

  case 18:
#line 247 "css_yacc.y"
    {
        (yyval.animation_rule) = (struct td_animation_rule *)malloc(sizeof(struct td_animation_rule));
        (yyval.animation_rule)->key = (yyvsp[(1) - (6)].lexeme);
        struct td_property *i = (yyvsp[(5) - (6)].property);
        while (i != NULL) {
            i->count++;
            i = i->next;
        }
        (yyval.animation_rule)->property = (yyvsp[(5) - (6)].property);
        (yyval.animation_rule)->next = NULL;
    ;}
    break;

  case 19:
#line 261 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 20:
#line 262 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 21:
#line 270 "css_yacc.y"
    { (yyval.property) = (yyvsp[(1) - (1)].property); ;}
    break;

  case 22:
#line 271 "css_yacc.y"
    { //多个属性
        if((yyvsp[(1) - (4)].property) != NULL) {
            (yyvsp[(1) - (4)].property)->next = (yyvsp[(4) - (4)].property);
            (yyval.property) = (yyvsp[(1) - (4)].property);
        }
        else {
            (yyval.property) = (yyvsp[(4) - (4)].property);
        }
    ;}
    break;

  case 23:
#line 284 "css_yacc.y"
    { (yyval.selector) = (yyvsp[(1) - (2)].selector); ;}
    break;

  case 24:
#line 288 "css_yacc.y"
    { (yyval.selector) = (yyvsp[(1) - (1)].selector); ;}
    break;

  case 25:
#line 289 "css_yacc.y"
    { (yyval.selector) = (yyvsp[(1) - (1)].selector); ;}
    break;

  case 26:
#line 290 "css_yacc.y"
    {  //后代选择器
        (yyval.selector) = (yyvsp[(1) - (1)].selector); 
    ;}
    break;

  case 27:
#line 293 "css_yacc.y"
    {  //子元素选择器
        (yyval.selector) = (yyvsp[(1) - (1)].selector);
    ;}
    break;

  case 28:
#line 296 "css_yacc.y"
    {
		struct td_selector *pos = (yyvsp[(1) - (2)].selector);
		while (pos->next != NULL) {
			pos = pos->next;
		}
		pos->pseudo_element = (yyvsp[(2) - (2)].pseudo_element);
		(yyval.selector) = (yyvsp[(1) - (2)].selector);
	;}
    break;

  case 29:
#line 304 "css_yacc.y"
    {
		struct td_selector *pos = (yyvsp[(1) - (2)].selector);
		while (pos->next != NULL) {
			pos = pos->next;
		}
		pos->next = (yyvsp[(2) - (2)].selector);
		(yyval.selector) = (yyvsp[(1) - (2)].selector);
	;}
    break;

  case 30:
#line 312 "css_yacc.y"
    { (yyval.selector) = (yyvsp[(1) - (1)].selector); ;}
    break;

  case 31:
#line 313 "css_yacc.y"
    { (yyval.selector) = NULL; ;}
    break;

  case 32:
#line 314 "css_yacc.y"
    { (yyval.selector) = NULL;;}
    break;

  case 33:
#line 318 "css_yacc.y"
    {  //后代选择器
        (yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
        (yyval.selector)->element_Sel = NULL;
        (yyval.selector)->id_Sel = NULL;
        (yyval.selector)->class_Sel = NULL;
        (yyval.selector)->attribute_Sel = NULL;
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = (yyvsp[(1) - (2)].selector);
        (yyval.selector)->child_Sel = NULL;
        (yyval.selector)->pseudo_class = 0;
        (yyval.selector)->pseudo_element = 0;
        (yyval.selector)->next = NULL;
        while ((yyval.selector)->descendant_Sel->next != NULL) {
            (yyval.selector)->descendant_Sel = (yyval.selector)->descendant_Sel->next;
        }
        (yyval.selector)->descendant_Sel->next = (yyvsp[(2) - (2)].selector);
     //   $$->child = $2;
    ;}
    break;

  case 34:
#line 348 "css_yacc.y"
    {
        (yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
        (yyval.selector)->element_Sel = NULL;
        (yyval.selector)->id_Sel = NULL;
        (yyval.selector)->class_Sel = NULL;
        (yyval.selector)->attribute_Sel = NULL;
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = NULL;
        (yyval.selector)->child_Sel = (yyvsp[(1) - (3)].selector);
        (yyval.selector)->pseudo_class = 0;
        (yyval.selector)->pseudo_element = 0;
        (yyval.selector)->next = NULL;
        while ((yyval.selector)->child_Sel->next != NULL) {
            (yyval.selector)->child_Sel = (yyval.selector)->child_Sel->next;
        }
        (yyval.selector)->child_Sel->next = (yyvsp[(3) - (3)].selector);
    //        char *s = (char *)malloc(strlen($1) + strlen($3) + 2);
    //        strcpy(s, $1);
    //        s[strlen(s) + 1] = 0;
    //        s[strlen(s)] = $2;
    //        strcat(s, $3);
    //        free($1);
    //        free($3);
    //        $$ = s;
    ;}
    break;

  case 35:
#line 378 "css_yacc.y"
    { 
        (yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
        (yyval.selector)->element_Sel = NULL;
        (yyval.selector)->id_Sel = NULL;
        (yyval.selector)->class_Sel = NULL;
        (yyval.selector)->attribute_Sel = (yyvsp[(1) - (1)].lexeme);
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = NULL;
        (yyval.selector)->child_Sel = NULL;
        (yyval.selector)->pseudo_class = 0;
        (yyval.selector)->pseudo_element = 0;
        (yyval.selector)->next = NULL;
     //   $$->child = NULL;
    ;}
    break;

  case 36:
#line 395 "css_yacc.y"
    {   /*元素选择器*/
        (yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
        (yyval.selector)->element_Sel = (yyvsp[(1) - (1)].lexeme);
        (yyval.selector)->id_Sel = NULL;
        (yyval.selector)->class_Sel = NULL;
        (yyval.selector)->attribute_Sel = NULL;
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = NULL;
        (yyval.selector)->child_Sel = NULL;
        (yyval.selector)->pseudo_class = 0;
        (yyval.selector)->pseudo_element = 0;
        (yyval.selector)->next = NULL;
     //   $$->child = NULL;
    ;}
    break;

  case 37:
#line 412 "css_yacc.y"
    { (yyval.selector) = (yyvsp[(1) - (1)].selector); ;}
    break;

  case 38:
#line 416 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 39:
#line 417 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 40:
#line 421 "css_yacc.y"
    { (yyval.selector) = (yyvsp[(1) - (1)].selector); ;}
    break;

  case 41:
#line 422 "css_yacc.y"
    {
        printf("############\n");
    ;}
    break;

  case 42:
#line 428 "css_yacc.y"
    {
		(yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
		(yyval.selector)->element_Sel = NULL;
		(yyval.selector)->id_Sel = (yyvsp[(1) - (1)].lexeme);
		(yyval.selector)->class_Sel = NULL;
        (yyval.selector)->attribute_Sel = NULL;
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = NULL;
        (yyval.selector)->child_Sel = NULL;
		(yyval.selector)->pseudo_class = 0;
		(yyval.selector)->pseudo_element = 0;
		(yyval.selector)->next = NULL;
     //   $$->child = NULL;
    ;}
    break;

  case 43:
#line 442 "css_yacc.y"
    {
        char *s = (char*)malloc(strlen((yyvsp[(1) - (2)].lexeme)) + strlen((yyvsp[(2) - (2)].lexeme)));
        strcpy(s,(yyvsp[(1) - (2)].lexeme));
        strcat(s,(yyvsp[(2) - (2)].lexeme));
        free((yyvsp[(1) - (2)].lexeme));
        free((yyvsp[(2) - (2)].lexeme));
        
        (yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
		(yyval.selector)->element_Sel = NULL;
		(yyval.selector)->id_Sel = s;
		(yyval.selector)->class_Sel = NULL;
        (yyval.selector)->attribute_Sel = NULL;
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = NULL;
        (yyval.selector)->child_Sel = NULL;
		(yyval.selector)->pseudo_class = 0;
		(yyval.selector)->pseudo_element = 0;
		(yyval.selector)->next = NULL;
     //   $$->child = NULL;
    ;}
    break;

  case 44:
#line 462 "css_yacc.y"
    {
		(yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
		(yyval.selector)->element_Sel = NULL;
		(yyval.selector)->id_Sel = NULL;
		(yyval.selector)->class_Sel = (yyvsp[(1) - (1)].lexeme);
        (yyval.selector)->attribute_Sel = NULL;
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = NULL;
        (yyval.selector)->child_Sel = NULL;
		(yyval.selector)->pseudo_class = 0;
		(yyval.selector)->pseudo_element = 0;
		(yyval.selector)->next = NULL;
     //   $$->child = NULL;
    ;}
    break;

  case 45:
#line 476 "css_yacc.y"
    {
        char *s = (char*)malloc(strlen((yyvsp[(1) - (2)].lexeme)) + strlen((yyvsp[(2) - (2)].lexeme)));
        strcpy(s,(yyvsp[(1) - (2)].lexeme));
        strcat(s,(yyvsp[(2) - (2)].lexeme));
        free((yyvsp[(1) - (2)].lexeme));
        free((yyvsp[(2) - (2)].lexeme));
        
        (yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
		(yyval.selector)->element_Sel = NULL;
		(yyval.selector)->id_Sel = NULL;
		(yyval.selector)->class_Sel = s;
        (yyval.selector)->attribute_Sel = NULL;
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = NULL;
        (yyval.selector)->child_Sel = NULL;
		(yyval.selector)->pseudo_class = 0;
		(yyval.selector)->pseudo_element = 0;
		(yyval.selector)->next = NULL;
     //   $$->child = NULL;
    ;}
    break;

  case 46:
#line 496 "css_yacc.y"
    { (yyval.selector) = (yyvsp[(1) - (1)].selector); ;}
    break;

  case 47:
#line 504 "css_yacc.y"
    { 
        (yyval.lexeme) = (char*)malloc(strlen((yyvsp[(2) - (2)].lexeme))+2);
        sprintf((yyval.lexeme),".%s",(yyvsp[(2) - (2)].lexeme));
    //        char *s = (char *)malloc(strlen($2) + 2);
    //        s[strlen(s)] = '.';
    //        strcat(s, $2);
        free((yyvsp[(2) - (2)].lexeme));
       // $$ = s;
        printf("------>  %s",(yyval.lexeme));
    ;}
    break;

  case 48:
#line 517 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 49:
#line 521 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 50:
#line 522 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 51:
#line 527 "css_yacc.y"
    { /* [name] */
        char *s = (char*)malloc(strlen((yyvsp[(3) - (4)].lexeme)) + 2);
        strcpy(s,(yyvsp[(1) - (4)].lexeme));
        strcat(s,(yyvsp[(3) - (4)].lexeme));
        strcat(s,(yyvsp[(4) - (4)].lexeme));
 //通过字符数组直接进行字符串拼接s[strlen(s)] = $3，编译后会出现乱码。换成strcat可正常显示               
 //        s[strlen(s)+1] = 0;
 //        s[strlen(s)] = $3;
 //        printf("%s,%zd\n",s,strlen(s));


        free((yyvsp[(3) - (4)].lexeme));
        (yyval.lexeme) = s;
    ;}
    break;

  case 52:
#line 541 "css_yacc.y"
    {
        /*[name="value"]*/
        char *s = (char*)malloc(strlen((yyvsp[(3) - (8)].lexeme)) + strlen((yyvsp[(5) - (8)].lexeme)) + 2);
        strcpy(s,(yyvsp[(1) - (8)].lexeme));
        strcat(s,(yyvsp[(3) - (8)].lexeme));
        s[strlen(s) + 1] = 0;
        s[strlen(s)] = (yyvsp[(4) - (8)].letter);
        strcat(s,(yyvsp[(6) - (8)].lexeme));
        strcat(s,(yyvsp[(8) - (8)].lexeme));
        
        free((yyvsp[(3) - (8)].lexeme));
        free((yyvsp[(6) - (8)].lexeme));
        (yyval.lexeme) = s;
    ;}
    break;

  case 53:
#line 558 "css_yacc.y"
    { (yyval.letter) = '='; ;}
    break;

  case 54:
#line 562 "css_yacc.y"
    { (yyval.pseudo_element) = PS_ELEMENT_FIRST_LETTER; ;}
    break;

  case 55:
#line 563 "css_yacc.y"
    { (yyval.pseudo_element) = PS_ELEMENT_FIRST_LINE; ;}
    break;

  case 56:
#line 567 "css_yacc.y"
    {
		(yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
		(yyval.selector)->element_Sel = NULL;
		(yyval.selector)->id_Sel = NULL;
		(yyval.selector)->class_Sel = NULL;
        (yyval.selector)->attribute_Sel = NULL;
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = NULL;
        (yyval.selector)->child_Sel = NULL;
		(yyval.selector)->pseudo_class = 0;
		(yyval.selector)->pseudo_element = PS_ELEMENT_FIRST_LETTER;
		(yyval.selector)->next = NULL;
     //   $$->child = NULL;
	;}
    break;

  case 57:
#line 581 "css_yacc.y"
    {
		(yyval.selector) = (struct td_selector*)malloc(sizeof(struct td_selector));
		(yyval.selector)->element_Sel = NULL;
		(yyval.selector)->id_Sel = NULL;
		(yyval.selector)->class_Sel = NULL;
        (yyval.selector)->attribute_Sel = NULL;
        (yyval.selector)->group_Sel = NULL;
        (yyval.selector)->descendant_Sel = NULL;
        (yyval.selector)->child_Sel = NULL;
		(yyval.selector)->pseudo_class = 0;
		(yyval.selector)->pseudo_element = PS_ELEMENT_FIRST_LINE;
		(yyval.selector)->next = NULL;
     //   $$->child = NULL;
	;}
    break;

  case 58:
#line 599 "css_yacc.y"
    {
        (yyval.property) = (struct td_property*)malloc(sizeof(struct td_property));
        (yyval.property)->name = (yyvsp[(1) - (5)].lexeme);
        (yyval.property)->val = (yyvsp[(4) - (5)].lexeme);
        (yyval.property)->important = 1;
        (yyval.property)->count = 0;
        (yyval.property)->next = NULL;
    ;}
    break;

  case 59:
#line 607 "css_yacc.y"
    {
        (yyval.property) = (struct td_property*)malloc(sizeof(struct td_property));
        (yyval.property)->name = (yyvsp[(1) - (4)].lexeme);
        (yyval.property)->val = (yyvsp[(4) - (4)].lexeme);
        (yyval.property)->important = 0;
        (yyval.property)->count = 0;
        (yyval.property)->next = NULL;
    ;}
    break;

  case 60:
#line 615 "css_yacc.y"
    { (yyval.property) = NULL; ;}
    break;

  case 61:
#line 616 "css_yacc.y"
    { (yyval.property) = NULL; ;}
    break;

  case 62:
#line 620 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme);;}
    break;

  case 63:
#line 624 "css_yacc.y"
    { ;}
    break;

  case 64:
#line 628 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 65:
#line 629 "css_yacc.y"
    {
        char *s = (char*)malloc(strlen((yyvsp[(1) - (3)].lexeme))+strlen((yyvsp[(3) - (3)].lexeme))+2);
        strcpy(s,(yyvsp[(1) - (3)].lexeme));
        s[strlen(s)+1] = 0;
        s[strlen(s)] = (yyvsp[(2) - (3)].letter);
        strcat(s,(yyvsp[(3) - (3)].lexeme));
        free((yyvsp[(1) - (3)].lexeme));
        free((yyvsp[(3) - (3)].lexeme));
        (yyval.lexeme) = s;
    ;}
    break;

  case 66:
#line 639 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 67:
#line 643 "css_yacc.y"
    {
        char *s = (char*)malloc(strlen((yyvsp[(2) - (2)].lexeme))+2);
        s[0] = (yyvsp[(1) - (2)].letter);
        s[1] = 0;
        strcat(s,(yyvsp[(2) - (2)].lexeme));
        free((yyvsp[(2) - (2)].lexeme));
        (yyval.lexeme) = s;
    ;}
    break;

  case 68:
#line 651 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 69:
#line 655 "css_yacc.y"
    { (yyval.letter) = '>'; ;}
    break;

  case 70:
#line 659 "css_yacc.y"
    { (yyval.letter) = '-'; ;}
    break;

  case 71:
#line 660 "css_yacc.y"
    { (yyval.letter) = '+'; ;}
    break;

  case 72:
#line 664 "css_yacc.y"
    { (yyval.letter) = '/'; ;}
    break;

  case 73:
#line 665 "css_yacc.y"
    { (yyval.letter) = ','; ;}
    break;

  case 74:
#line 666 "css_yacc.y"
    { (yyval.letter) = ' '; ;}
    break;

  case 75:
#line 670 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 76:
#line 671 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 77:
#line 672 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 78:
#line 673 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 79:
#line 674 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 80:
#line 675 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 81:
#line 676 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 82:
#line 677 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (1)].lexeme); ;}
    break;

  case 83:
#line 678 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 84:
#line 679 "css_yacc.y"
    { (yyval.lexeme) = (yyvsp[(1) - (2)].lexeme); ;}
    break;

  case 85:
#line 683 "css_yacc.y"
    {
        (yyval.lexeme) = (char*)malloc(strlen((yyvsp[(1) - (2)].lexeme))+2);
        sprintf((yyval.lexeme),"%s",(yyvsp[(1) - (2)].lexeme));
        free((yyvsp[(1) - (2)].lexeme));
    ;}
    break;

  case 86:
#line 688 "css_yacc.y"
    {
        (yyval.lexeme) = (char*)malloc(strlen((yyvsp[(1) - (2)].lexeme))+2);
        sprintf((yyval.lexeme),"%s",(yyvsp[(1) - (2)].lexeme));
        free((yyvsp[(1) - (2)].lexeme));
    ;}
    break;


/* Line 1267 of yacc.c.  */
#line 2341 "css_yacc.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 695 "css_yacc.y"


int yyerror(char *s) {
#if YYDEBUG
	fprintf(stderr,"Error:%s\n",s);
#endif
	return 0;
}

struct td_selector_list *css_parse(const char *buffer, int buf_len) {
	struct td_selector_list *ret = NULL;
	init_yylex(buffer,buf_len);
	yyparse(&ret);
	end_yylex();
	return ret;
}
