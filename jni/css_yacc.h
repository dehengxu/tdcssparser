/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     UNIMPORTANT_TOK = 258,
     WHITESPACE = 259,
     SGML_CD = 260,
     WEBKIT_KEYFRAMES_RULE_SYM = 261,
     WEBKIT_KEYFRAMES_SYM = 262,
     STRING = 263,
     NUMBER = 264,
     PERCENTAGE = 265,
     LENGTH = 266,
     EMS = 267,
     EXS = 268,
     IDENT = 269,
     LINK_PSCLASS_AFTER_IDENT = 270,
     VISITED_PSCLASS_AFTER_IDENT = 271,
     ACTIVE_PSCLASS_AFTER_IDENT = 272,
     FIRST_LINE_AFTER_IDENT = 273,
     FIRST_LETTER_AFTER_IDENT = 274,
     HASH_AFTER_IDENT = 275,
     CLASS_AFTER_IDENT = 276,
     LINK_PSCLASS = 277,
     VISITED_PSCLASS = 278,
     ACTIVE_PSCLASS = 279,
     FIRST_LINE = 280,
     FIRST_LETTER = 281,
     IDSEL = 282,
     CLASS = 283,
     URL = 284,
     RGB = 285,
     IMPORTANT_SYM = 286,
     LBRACKET = 287,
     RBRACKET = 288,
     ASTERISK = 289
   };
#endif
/* Tokens.  */
#define UNIMPORTANT_TOK 258
#define WHITESPACE 259
#define SGML_CD 260
#define WEBKIT_KEYFRAMES_RULE_SYM 261
#define WEBKIT_KEYFRAMES_SYM 262
#define STRING 263
#define NUMBER 264
#define PERCENTAGE 265
#define LENGTH 266
#define EMS 267
#define EXS 268
#define IDENT 269
#define LINK_PSCLASS_AFTER_IDENT 270
#define VISITED_PSCLASS_AFTER_IDENT 271
#define ACTIVE_PSCLASS_AFTER_IDENT 272
#define FIRST_LINE_AFTER_IDENT 273
#define FIRST_LETTER_AFTER_IDENT 274
#define HASH_AFTER_IDENT 275
#define CLASS_AFTER_IDENT 276
#define LINK_PSCLASS 277
#define VISITED_PSCLASS 278
#define ACTIVE_PSCLASS 279
#define FIRST_LINE 280
#define FIRST_LETTER 281
#define IDSEL 282
#define CLASS 283
#define URL 284
#define RGB 285
#define IMPORTANT_SYM 286
#define LBRACKET 287
#define RBRACKET 288
#define ASTERISK 289




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 15 "css_yacc.y"
{
	char *lexeme;
	char letter;
	struct td_property *property;
	struct td_selector *selector;
	struct td_selector_list *selector_list;
    struct td_descendant_name *descendant_name;
	int pseudo_class;
	int pseudo_element;
    struct td_animation_rule *animation_rule;
    struct td_keyframes_rule *keyframesRule;
}
/* Line 1529 of yacc.c.  */
#line 130 "css_yacc.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



