LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_LDLIBS += -L$(SYSROOT)/usr/lib -llog

LOCAL_MODULE    := NDK_CSSParser
LOCAL_SRC_FILES := \
GetCssparser.c	\
css_lex.c	\
css_yacc.c

include $(BUILD_SHARED_LIBRARY)